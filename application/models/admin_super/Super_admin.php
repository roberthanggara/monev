<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_admin extends CI_Model {
#--------------------------admin_lv-----------------------

	public function get_lv_admin($where){
        $data = $this->db->get_where("admin_lv", $where)->result();
        return $data;
	}

    public function insert_lv_admin($data){
        $insert = $this->db->insert("admin_lv", $data);
        return $insert;
    }

    public function update_lv_admin($set, $where){
        $update = $this->db->update("admin_lv", $set, $where);
        return $update;
    }
    
    public function delete_lv_admin($where){
        $delete = $this->db->delete("admin_lv", $where);
        return $delete;
	}

#--------------------------super_dinas--------------------

	public function get_dinas(){
        $data = $this->db->get("dinas")->result();
        return $data;
	}

    public function get_dinas_active(){
        $this->db->where("is_del", "0");
        $data = $this->db->get("dinas")->result();
        return $data;
    }
    
    public function get_where_dinas($where){
        $data = $this->db->get_where("dinas", $where)->row_array();
        return $data;
	}
    
    public function insert_dinas($data){
        $insert = $this->db->insert("dinas", $data);
        return $insert;
	}
    
    public function update_dinas($set, $where){
        $update = $this->db->update("dinas", $set, $where);
        return $update;
	}
    
    public function delete_dinas($where){
        $delete = $this->db->delete("dinas", $where);
        return $delete;
	}

#--------------------------super_jenis_doc---------------

    public function get_jenis_doc(){
        $data = $this->db->get("jenis_doc")->result();
        return $data;
    }

    public function get_jenis_doc_active(){
        $this->db->where("is_del", "0");
        $data = $this->db->get("jenis_doc")->result();
        return $data;
    }
    
    public function get_where_jenis_doc($where){
        $data = $this->db->get_where("jenis_doc", $where)->row_array();
        return $data;
    }
    
    public function insert_jenis_doc($data){
        $insert = $this->db->insert("jenis_doc", $data);
        return $insert;
    }
    
    public function update_jenis_doc($set, $where){
        $update = $this->db->update("jenis_doc", $set, $where);
        return $update;
    }
    
    public function delete_jenis_doc($where){
        $delete = $this->db->delete("jenis_doc", $where);
        return $delete;
    }
    
#--------------------------super_user---------------------

	public function get_user(){
	    $this->db->select("id_user, email, nama, alamat, nik, tlp, status_active, pekerjaan, instansi");
        $data = $this->db->get("user")->result();
        return $data;
	}

    public function get_user_active(){
        $this->db->select("id_user, email, nama, alamat, nik, tlp, status_active, pekerjaan, instansi");
        $this->db->where("is_del", "0");
        $data = $this->db->get("user")->result();
        return $data;
    }
    
    public function get_where_user($where){
        $this->db->select("id_user, email, nama, alamat, nik, tlp, status_active, pekerjaan, instansi");
        $data = $this->db->get_where("user", $where)->row_array();
        return $data;
	}
    
    public function update_user($set, $where){
        $update = $this->db->update("user", $set, $where);
        return $update;
	}

    public function delete_user($where){
        $delete = $this->db->delete("user", $where);
        return $delete;
    }

#--------------------------super_admin--------------------

	public function get_admin(){
	    $this->db->select("id_admin, email, nama, nama_dinas, ket, jabatan");
	    $this->db->join("dinas d", "ad.id_bidang = d.id_dinas");
        $this->db->join("admin_lv al", "ad.id_lv = al.id_lv");
        $data = $this->db->get("admin ad")->result();
        return $data;
	}

    public function get_admin_all(){
        $this->db->select("id_admin, email, nama, nama_dinas, ket, jabatan");
        $this->db->join("dinas d", "ad.id_bidang = d.id_dinas");
        $this->db->join("admin_lv al", "ad.id_lv = al.id_lv");

        $this->db->where("ad.is_del", "0");
        $data = $this->db->get("admin ad")->result();
        return $data;
    }
    
    public function get_where_admin($where){
        $this->db->select("id_admin, email, nama, id_dinas, nama_dinas, ket, jabatan, ad.id_lv, password, nip");
        $this->db->join("dinas d", "ad.id_bidang = d.id_dinas");
        $this->db->join("admin_lv al", "ad.id_lv = al.id_lv");
        $data = $this->db->get_where("admin ad", $where)->result();
        return $data;
	}

    public function get_where_admin_ex($where){
        $data = $this->db->get_where("admin", $where)->result();
        return $data;
    }
    
    public function insert_admin($data){
        $insert = $this->db->insert("admin", $data);
        return $insert;
	}
    
    public function update_admin($set, $where){
        $update = $this->db->update("admin", $set, $where);
        return $update;
	}
    
    public function delete_admin($where){
        $delete = $this->db->delete("admin", $where);
        return $delete;
	}

#--------------------------super_pemohon------------------

    public function get_pemohon_active($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon_new pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        $this->db->where("pe.is_del", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }
    
    public function delete_pemohon($where){
        $delete = $this->db->delete("pemohon_new", $where);
        return $delete;
	}

    public function update_pemohon($set, $where){
        $update = $this->db->update("pemohon_new", $set, $where);
        return $update;
    }

  
}
