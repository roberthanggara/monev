<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Model{
    
    public function get_admin($where){
        $this->db->select("a.id_lv, al.ket, email, status_active, nama, jabatan, id_bidang");
        $this->db->join("admin_lv al", "al.id_lv = a.id_lv");
        $data = $this->db->get_where("admin a", $where)->row_array();
        return $data;
    }
    
    public function get_pemohon($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
	    $this->db->join('user us','pe.id_user=us.id_user');
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        $this->db->where("pe.status_magang", "1");
        $this->db->where("pe.status_diterima", "0");
        $this->db->where("pe.status_m", "0");
        $this->db->where("pe.instansi_penerima", "0");
        //$this->db->where("pe.no_register", "0");
        $query = $this->db->get()->result();
        return $query;
    }

    public function get_pemohon_surat($where){
        $this->db->select('*');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
        $this->db->join('user us','pe.id_user=us.id_user');
        $this->db->join('doc_pemohon dp','pe.id_pemohon=dp.id_pemohon');

        $query = $this->db->get_where('pemohon pe', $where)->row_array();
        //$this->db->where("pe.no_register", "0");
        //$query = $this->db->get()->row_array();
        return $query;
    }
    
    public function get_pemohon_reg($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
	    $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        
        $this->db->where("pe.status_magang", "1");
        $this->db->where("pe.no_register!=", "0");
        $this->db->where("pe.tgl_selesai >=", date("Y-m-d"));
        $this->db->where("pe.status_diterima", "1");
        $this->db->where("pe.status_m", "1");
        $this->db->where("pe.instansi_penerima!=", "0");
        //$this->db->where("pe.no_register !=", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }
    
    public function get_pemohon_report($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
	    $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        
        $this->db->where("pe.status_magang", "1");
        $this->db->where("pe.no_register!=", "0");
        $this->db->where("pe.status_diterima!=", "0");
        $this->db->where("pe.status_m!=", "0");
        //$this->db->where("pe.tgl_selesai >=", date("Y-m-d"));
        //$this->db->where("pe.no_register !=", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }
    
    public function cek_no_reg($where){
        $this->db->select("no_register, id_permohonan");
        return $this->db->get_where("pemohon", $where)->row_array();
    }

	public function update_register($id_pemohon, $permohonan){
        $this->db->query("call update_no_reg('$id_pemohon', '$permohonan');");
    }
    
#-----------------------------------------------------------------OPD--------------------------------------------------------------------

    public function list_opd_acc($jenis_pemohon){
        $this->db->select('*');
        $this->db->from('pemohon pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
	    $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        
        $this->db->where("pe.status_magang", "1");
        $this->db->where("pe.status_diterima", "0");
        $this->db->where("pe.status_m", "0");
        $this->db->where("pe.no_register!=", "0");
        //$this->db->where("pe.tgl_selesai >=", date("Y-m-d"));
        //$this->db->where("pe.no_register !=", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }
    
    public function list_opd_history($jenis_pemohon,$id_instansi){
        $this->db->select('*');
        $this->db->from('pemohon pe');
        $this->db->join('permohonan pr','pr.id_permohonan=pe.id_permohonan');
	    $this->db->join('user us','pe.id_user=us.id_user');
        
        $this->db->where_in('pr.jenis_kegiatan',$jenis_pemohon);
        
        $this->db->where("pe.status_magang", "1");
        $this->db->where("pe.no_register!=", "0");
        $this->db->where("pe.status_diterima !=", "0");
        $this->db->where("pe.status_m !=", "0");
        $this->db->where("pe.instansi_penerima=", $id_instansi);
        
        //$this->db->where("pe.tgl_selesai >=", date("Y-m-d"));
        //$this->db->where("pe.no_register !=", "0");
        
        $query = $this->db->get()->result();
        return $query;
    }
    
    public function acc_pemohon($set,$where){
        $data = $this->db->update("pemohon", $set, $where);
        return $data;
    }

    public function get_dinas(){
        $dinas = $this->db->get("dinas")->result();
        return $dinas;
    }

    public function get_anggota($where){
        $anggota = $this->db->get_where("anggota", $where)->result();
        return $anggota;
    }
    
    public function get_pemohon_where($where){
        $pemohon = $this->db->get_where("pemohon", $where)->row_array();
        return $pemohon;
    }
}
?>