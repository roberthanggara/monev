<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_new extends CI_Model{
    
    public function get_admin($where){
        $this->db->select("id_admin, a.id_lv, al.ket, email, status_active, nama, jabatan, a.id_bidang, dn.nama_dinas");
        $this->db->join("admin_lv al", "al.id_lv = a.id_lv");
        $this->db->join("dinas dn", "dn.id_dinas = a.id_bidang");
        $data = $this->db->get_where("admin a", $where)->row_array();
        return $data;
    }

#--------------------------------------------dokumen--------------------------------------------

    public function get_doc_all(){
        $data = $this->db->get("doc")->result();
        return $data;
    }

    public function get_doc_where($where){
        $data = $this->db->get_where("doc", $where)->result();
        return $data;
    }

    public function get_doc_where_fush($where){
        $this->db->join("admin ad", "dc.id_admin = ad.id_admin");
        $this->db->join("dinas dn", "ad.id_bidang = dn.id_dinas");
        $this->db->join("jenis_doc jd", "dc.id_jenis = jd.id_jenis");
        $data = $this->db->get_where("doc dc", $where)->result();
        return $data;
    }

    public function get_doc_where_fush_each($where){
        $this->db->join("admin ad", "dc.id_admin = ad.id_admin");
        $this->db->join("dinas dn", "ad.id_bidang = dn.id_dinas");
        $this->db->join("jenis_doc jd", "dc.id_jenis = jd.id_jenis");
        $data = $this->db->get_where("doc dc", $where)->row_array();
        return $data;
    }

    public function get_doc_where_each($where){
        $data = $this->db->get_where("doc", $where)->row_array();
        return $data;
    }

    public function insert_doc($data){
        $insert = $this->db->insert("doc", $data);
        return $insert;
    }

    public function update_doc($set, $where){
        $update = $this->db->update("doc", $set, $where);
        return $update;
    }
    
    public function delete_doc($where){
        $delete = $this->db->delete("doc", $where);
        return $delete;
    } 

#--------------------------super_jenis_doc---------------

    public function get_jenis_doc(){
        $data = $this->db->get("jenis_doc")->result();
        return $data;
    }

    public function get_jenis_doc_active(){
        $this->db->where("is_del", "0");
        $data = $this->db->get("jenis_doc")->result();
        return $data;
    }
    
    public function get_where_jenis_doc($where){
        $data = $this->db->get_where("jenis_doc", $where)->row_array();
        return $data;
    }
    
    public function insert_jenis_doc($data){
        $insert = $this->db->insert("jenis_doc", $data);
        return $insert;
    }
    
    public function update_jenis_doc($set, $where){
        $update = $this->db->update("jenis_doc", $set, $where);
        return $update;
    }
    
    public function delete_jenis_doc($where){
        $delete = $this->db->delete("jenis_doc", $where);
        return $delete;
    }
}
?>