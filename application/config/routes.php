<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route["beranda"] 				= "front/frontmain/index";
$route["variabel_kepatuahan"] 	= "front/frontmain/var_kepatuhan";

$route["back-admin/login"] 			= "admin_new/adminlogin/index";
$route["back-admin/logout"] 		= "admin_new/adminall/logout";

//admin super
$route["super/home"] 			= "admin_super/superadmin/index";
$route["super/dinas"] 			= "admin_super/superadmin/index_dinas";
$route["super/admin"] 			= "admin_super/superadmin/index_admin";
$route["super/lv"] 				= "admin_super/superadmin/index_admin_lv";
$route["super/jenis_dokumen"] 	= "admin_super/superadmin/index_jenis_doc";

$route["super/pemeriksaan_wait"] 		= "admin_super/superadmin/index_pemeriksaan_wait";
$route["super/pemeriksaan_no"] 			= "admin_super/superadmin/pemeriksaan_no";

//action super
$route["super/admin_lv/add_"]["post"]		= "admin_super/superadmin/insert_admin_lv";
$route["super/admin_lv/delete_"]["post"]	= "admin_super/superadmin/delete_admin_lv";

$route["super/putpass_"]["post"]	= "admin_super/superadmin/get_password";
$route["super/dontkillme_"]["post"]	= "admin_super/superadmin/change_pass";

//admin dinas
$route["dinas/home"] 				= "admin_new/adminmain/index";
$route["dinas/dokumen/(:num)"] 		= "admin_new/adminmain/index_doc/$1";
$route["dinas/hasil_penilaian/(:num)"] 	= "admin_new/adminmain/index_hasil/$1";
