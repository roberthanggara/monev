<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Superadmin extends CI_Controller {

    public function __construct(){
        parent::__construct();  
        $this->load->model('admin_super/super_admin', 'as');
        $this->load->model('admin/admin_new', 'adn');
        
        $this->load->library("response_message");
        
        //print_r("<pre>");
        //print_r($_SESSION);
        $session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"] != 1 && $session["id_lv"] != 1 && $session["is_log"] != 1){
                redirect(base_url()."back-admin/login");
            }
        }else{
            redirect(base_url()."back-admin/login");
        }
    }


#=============================================================================#
#-------------------------------------------Index_Home------------------------#
#=============================================================================#
    public function index(){
        $data["dinas"] = $this->as->get_dinas();

        $data["page"] = "super_home";
        $data["t_no_respons"]   = count($this->adn->get_doc_where(array("sts_check"=>"0")));
        $data["t_acc"]          = count($this->adn->get_doc_where(array("sts_check"=>"1")));
        $data["t_no_acc"]       = count($this->adn->get_doc_where(array("sts_check"=>"2")));

        $this->load->view('admin_super_new',$data);
    }
#=============================================================================#
#-------------------------------------------Index_Home------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#
    public function index_admin(){
        $data["admin"]  = $this->as->get_admin_all();
        $data["dinas"]  = $this->as->get_dinas();
        $data["lv"]     = $this->as->get_lv_admin(array("is_del"=>"0"));
        $data["page"]   = "super_admin";

        $this->load->view('admin_super_new',$data);
    }

    public function val_form(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails|is_unique[admin.email]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )
                       
                ),
                array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'nip',
                    'label'=>'Nomor Induk Pegawai',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'jbtn',
                    'label'=>'Jabatan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'dinas',
                    'label'=>'Nama Dinas',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'lv',
                    'label'=>'Lv. Admin',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'pass',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'repass',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "nama"=>"",
                    "nip"=>"",
                    "jabatan"=>"",
                    "id_bidang"=>"",
                    "lv"=>"",
                    "pass"=>"",
                    "repass"=>""
                );

        if($this->val_form()){
            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $nip = $this->input->post("nip");
            $jabatan = $this->input->post("jbtn");
            $id_bidang = $this->input->post("dinas");
            $lv = $this->input->post("lv");
            $pass = $this->input->post("pass");
            $repass = $this->input->post("repass");

            $admin_del = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update = date("Y-m-d h:i:s");

            if ($pass == $repass) {
                // print_r($_POST);
                $insert = $this->db->query("select func_insert_admin('".$nama."','".$email."','".$nip."','".$jabatan."','".$id_bidang."','".$lv."','".md5($pass)."','".$admin_del."','".$time_update."')");

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail = array(
                            "email"=>strip_tags(form_error('email')),
                            "nama"=>strip_tags(form_error('nama')),
                            "nip"=>strip_tags(form_error('nip')),
                            "jabatan"=>strip_tags(form_error('jbtn')),
                            "id_bidang"=>strip_tags(form_error('dinas')),
                            "lv"=>strip_tags(form_error('lv')),
                            "pass"=>strip_tags(form_error('pass')),
                            "repass"=>strip_tags(form_error('repass'))
                        );
            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
        // $this->session->set_flashdata("response_user", $res_msg);
        // redirect(base_url()."super/admin");
    }
    

    public function get_admin_update(){
        $id = $this->input->post("id_admin");
        // print_r($id);
        $data = $this->as->get_where_admin(array("id_admin"=>$id));
        // print_r($data);
        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }
    
    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )
                       
                ),
                array(
                    'field'=>'nama',
                    'label'=>'Nama',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'nip',
                    'label'=>'Nomor Induk Pegawai',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'jbtn',
                    'label'=>'Jabatan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'dinas',
                    'label'=>'Nama Dinas',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'lv',
                    'label'=>'Lv. Admin',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "email"=>"",
                    "nama"=>"",
                    "nip"=>"",
                    "jabatan"=>"",
                    "id_bidang"=>"",
                    "lv"=>""
                );

        if($this->val_form_update()){
            $id_admin = $this->input->post("id_admin");

            $nama = $this->input->post("nama");
            $email = $this->input->post("email");
            $nip = $this->input->post("nip");
            $jabatan = $this->input->post("jbtn");
            $id_bidang = $this->input->post("dinas");
            $lv = $this->input->post("lv");

            $admin_del = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update = date("Y-m-d h:i:s");

            if($this->as->get_where_admin_ex(array("email"=>$email, "id_admin!="=>$id_admin))){
                $msg_detail["email"] = "email sudah terdaftar, silahkan gunakan email yang belum terdaftar";
            }else{
                $set = array(
                        "nama"=>$nama,
                        "email"=>$email,
                        "nip"=>$nip,
                        "jabatan"=>$jabatan,
                        "id_bidang"=>$id_bidang,
                        "id_lv"=>$lv,
                        "admin_del"=>$admin_del,
                        "time_update"=>$time_update
                    );

                $where = array(
                            "id_admin"=>$id_admin
                        );

                $update = $this->as->update_admin($set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }            
        }else{
            $msg_detail["email"] = strip_tags(form_error('email'));
            $msg_detail["nama"] = strip_tags(form_error('nama'));
            $msg_detail["nip"] = strip_tags(form_error('nip'));
            $msg_detail["jabatan"] = strip_tags(form_error('jbtn'));
            $msg_detail["id_bidang"] = strip_tags(form_error('dinas'));
            $msg_detail["lv"] = strip_tags(form_error('lv'));
                            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'id',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete()){
            $id_admin = $this->input->post("id_admin");

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_del"=>$is_del,
                    "time_del"=>$time_del
                );

            $where = array("id_admin"=>$id_admin);

            if($this->as->update_admin($set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }


    public function get_password(){
        $res_array = array(
                    "status"=>false,
                    "val_key"=>""
                );

        if(isset($_POST["id_admin"])){
            $id_admin = $this->input->post("id_admin");
            $data = $this->as->get_where_admin_ex(array("id_admin"=>$id_admin));

            $res_array = array(
                    "status"=>true,
                    "val_key"=>hash('sha256', $data[0]->password)
                );
        }
        print_r(json_encode($res_array));
    }

    private function validation_change_pass(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'id',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'key',
                    'label'=>'key',
                    'rules'=>'required|alpha_numeric|exact_length[64]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR"),
                        'exact_length[64]'=>"%s ".$this->response_message->get_error_msg("PASSWORD_LENGHT")
                    )
                       
                ),


                array(
                    'field'=>'pass',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'repass',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "pass"=>"",
                    "repass"=>""
                );

        if($this->validation_change_pass()){
            $id_admin = $this->input->post("id_admin");
            $key = $this->input->post("key");

            $pass = $this->input->post("pass");
            $repass = $this->input->post("repass");

            if($pass == $repass){
                $data_admin = $this->as->get_where_admin_ex(array("id_admin"=>$id_admin));
                if(hash('sha256', $data_admin[0]->password) == $key){
                    $set = array(
                            "password"=>md5($pass),
                        );

                    $where = array(
                                "id_admin"=>$id_admin
                            );

                    $update = $this->as->update_admin($set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }else{
                    // $msg_main = array("status"=>false, "msg"=>"key salah");
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
            }            
        }else{
            // $msg_detail["id_admin"] = strip_tags(form_error('id_admin'));
            // $msg_detail["key"] = strip_tags(form_error('key'));

            $msg_detail["pass"] = strip_tags(form_error('pass'));
            $msg_detail["repass"] = strip_tags(form_error('repass'));                            
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Admin-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Lv_Admin--------------------#
#=============================================================================#
    public function index_admin_lv(){
        $data["lv"]     = $this->as->get_lv_admin(array("is_del"=>"0"));
        $this->load->view('admin_super_new/super_admin_lv',$data);
    }

    public function validation_admin_lv(){
         $config_val_input = array(
                array(
                    'field'=>'ket_lv',
                    'label'=>'Keterangan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin_lv(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "ket_lv"=>""
                    );
        if($this->validation_admin_lv()){
            $ket_lv = $this->input->post("ket_lv");
            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];

            $data = array(
                        "id_lv"=>"",
                        "ket"=>$ket_lv,
                        "is_del"=>"0",
                        "time_del"=>"0000-00-00 00:00:00.000000",
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            if($this->as->insert_lv_admin($data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else{
            $msg_detail = array(
                        "ket_lv"=>strip_tags(form_error("ket_lv"))
                    );
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        $this->session->set_flashdata("response_lv", $res_msg);
        redirect(base_url()."admin_super/superadmin/index_admin_lv");
    }

    public function delete_admin_lv(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));

        $id_lv = $this->input->post("id_lv");
        $data = array(
                    "is_del"=>"1",
                    "time_del"=>date("Y-m-d h:i:s")
                );
        if($this->as->update_lv_admin($data, array("id_lv"=>$id_lv))){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        }
        

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        $this->session->set_flashdata("response_lv", $res_msg);
        redirect(base_url()."admin_super/superadmin/index_admin_lv");
     }
#=============================================================================#
#-------------------------------------------Index_Lv_Admin--------------------#
#=============================================================================#    
    
#=============================================================================#
#-------------------------------------------Index_Dinas-----------------------#
#=============================================================================#
  
    public function index_dinas(){
        $data["dinas"] = $this->as->get_dinas_active();
        $data["page"] = "super_dinas";

        // print_r($data);
        $this->load->view('admin_super_new',$data);
    }
    
    public function validate_dinas(){
        $config_val_input = array(
                array(
                    'field'=>'nm_dinas',
                    'label'=>'Nama Dinas',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),
                array(
                    'field'=>'alamat',
                    'label'=>'Alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_dinas(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "nm_dinas"=>"",
                        "alamat"=>""
                    );
        if($this->validate_dinas()){
            $nm_dinas = $this->input->post("nm_dinas");
            $alamat = $this->input->post("alamat");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];

            $data = array(
                        "nama_dinas"=>$nm_dinas,
                        "alamat"=>$alamat,
                        "is_del"=>"0",
                        "time_del"=>"0000-00-00 00:00:00.000000",
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );
            if($this->as->insert_dinas($data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else {
            $msg_detail["nm_dinas"] = strip_tags(form_error('nm_dinas'));
            $msg_detail["alamat"] = strip_tags(form_error('alamat'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get_dinas_update(){
        $id = $this->input->post("id_dinas");
        // print_r($id);
        $data = $this->as->get_where_dinas(array("id_dinas"=>$id));
        // print_r($data);
        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));

    }


    public function update_dinas(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                        "nm_dinas"=>"",
                        "alamat"=>""
                    );
        if($this->validate_dinas()){
            $nm_dinas = $this->input->post("nm_dinas");
            $alamat = $this->input->post("alamat");

            $id_dinas = $this->input->post("id_dinas");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"]; 

            $data = array(
                        "nama_dinas"=>$nm_dinas,
                        "alamat"=>$alamat,
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                    "id_dinas"=>$id_dinas
                );
            if($this->as->update_dinas($data, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else {
            $msg_detail["nm_dinas"] = strip_tags(form_error('nm_dinas'));
            $msg_detail["alamat"] = strip_tags(form_error('alamat'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function delete_dinas(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $res_msg = $this->response_message->default_mgs($msg_main, null);

        if(isset($_POST["id_dinas"])){

            $id_dinas = $this->input->post("id_dinas");
            $data = array(
                        "is_del"=>"1",
                        "time_del"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                        "id_dinas"=>$id_dinas
                    );

            $update = $this->as->update_dinas($data, $where);

            if($update){
               $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
               
            }else{
               $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
            }
            
        }
        
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Dinas-----------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_Jenis_Permohonan------------#
#=============================================================================#
    public function index_permohonan(){
        $data["permohonan"] = $this->as->get_permohonan_active();
        $data["page"] = "super_permohonan";

        $this->load->view('admin_super_new',$data);
    }
    
    public function validate_permohonan(){
        $config_val_input = array(
                array(
                    'field'=>'jenis_kegiatan',
                    'label'=>'Jenis Kegiatan',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'keterangan_permohonan',
                    'label'=>'Keterangan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_permohonan(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "jenis_kegiatan"=>"",
                        "keterangan_permohonan"=>""
                    );
        if($this->validate_permohonan()){
            $jenis_kegiatan = $this->input->post("jenis_kegiatan");
            $keterangan_permohonan = $this->input->post("keterangan_permohonan");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];

            $data = array(
                        "id_permohonan"=>"",
                        "jenis_kegiatan"=>$jenis_kegiatan,
                        "keterangan_permohonan"=>$keterangan_permohonan,
                        "is_del"=>"0",
                        "time_del"=>"0000-00-00 00:00:00.000000",
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );
            if($this->as->insert_permohonan($data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else {
            $msg_detail["jenis_kegiatan"] = strip_tags(form_error('jenis_kegiatan'));
            $msg_detail["keterangan_permohonan"] = strip_tags(form_error('keterangan_permohonan'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get_permohonan_update(){
        $id = $this->input->post("id_permohonan");
        $data = $this->as->get_where_permohonan(array("id_permohonan"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));

    }


    public function update_permohonan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                        "jenis_kegiatan"=>"",
                        "keterangan_permohonan"=>""
                    );
        if($this->validate_permohonan()){
            $jenis_kegiatan = $this->input->post("jenis_kegiatan");
            $keterangan_permohonan = $this->input->post("keterangan_permohonan");

            $id_permohonan = $this->input->post("id_permohonan");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"]; 

            $data = array(
                        "jenis_kegiatan"=>$jenis_kegiatan,
                        "keterangan_permohonan"=>$keterangan_permohonan,
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                    "id_permohonan"=>$id_permohonan
                );
            if($this->as->update_permohonan($data, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else {
            $msg_detail["jenis_kegiatan"] = strip_tags(form_error('jenis_kegiatan'));
            $msg_detail["keterangan_permohonan"] = strip_tags(form_error('keterangan_permohonan'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function delete_permohonan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $res_msg = $this->response_message->default_mgs($msg_main, null);

        if(isset($_POST["id_permohonan"])){

            $id_permohonan = $this->input->post("id_permohonan");
            $data = array(
                        "is_del"=>"1",
                        "time_del"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                        "id_permohonan"=>$id_permohonan
                    );

            $update = $this->as->update_permohonan($data, $where);

            if($update){
               $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
               
            }else{
               $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
            }
            
        }
        
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_Jenis_Permohonan------------#
#=============================================================================# 

#=============================================================================#
#-------------------------------------------Index_jenis_document--------------#
#=============================================================================#
    public function index_jenis_doc(){
        $data["jenis_dokumen"] = $this->as->get_jenis_doc_active();
        $data["page"] = "super_jenis_doc";

        $this->load->view('admin_super_new',$data);
    }
    
    public function validate_jenis_doc(){
        $config_val_input = array(
                array(
                    'field'=>'keterangan_jenis_dokumen',
                    'label'=>'Keterangan',
                    'rules'=>'required|alpha_numeric_spaces',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric_spaces'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                ),array(
                    'field'=>'point',
                    'label'=>'Point',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_jenis_doc(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                        "keterangan_jenis_dokumen"=>"",
                        "point"=>""
                    );
        if($this->validate_jenis_doc()){
            $keterangan_jenis_doc = $this->input->post("keterangan_jenis_dokumen");
            $point = $this->input->post("point");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];

            $data = array(
                        "id_jenis"=>"",
                        "point"=>$point,
                        "keterangan_jenis"=>$keterangan_jenis_doc,
                        "is_del"=>"0",
                        "time_del"=>"0000-00-00 00:00:00.000000",
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );
            if($this->as->insert_jenis_doc($data)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else {
            $msg_detail["keterangan_jenis_dokumen"] = strip_tags(form_error('keterangan_jenis_dokumen'));
            $msg_detail["point"] = strip_tags(form_error('point'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_jenis_doc_update(){
        $id = $this->input->post("id_jenis");
        $data = $this->as->get_where_jenis_doc(array("id_jenis"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
            // $data_json = ;
        }

        print_r(json_encode($data_json));
    }

    public function update_jenis_doc(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                        "keterangan_jenis_dokumen"=>"",
                        "point"=>""
                    );
        if($this->validate_jenis_doc()){
            $keterangan_jenis_doc = $this->input->post("keterangan_jenis_dokumen");
            $point = $this->input->post("point");
            $id_jenis_doc = $this->input->post("id_jenis");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"]; 

            $data = array(
                        "keterangan_jenis"=>$keterangan_jenis_doc,
                        "point"=>$point,
                        "admin_del"=>$id_admin,
                        "time_update"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                    "id_jenis"=>$id_jenis_doc
                );
            if($this->as->update_jenis_doc($data, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else {
            $msg_detail["keterangan_jenis_dokumen"] = strip_tags(form_error('keterangan_jenis_dokumen'));
            $msg_detail["point"] = strip_tags(form_error('point'));
        }

        // redirect("super/dinas");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function delete_jenis_doc(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $res_msg = $this->response_message->default_mgs($msg_main, null);

        if(isset($_POST["id_jenis"])){

            $id_jenis = $this->input->post("id_jenis");
            $data = array(
                        "is_del"=>"1",
                        "time_del"=>date("Y-m-d h:i:s")
                    );

            $where = array(
                        "id_jenis"=>$id_jenis
                    );

            $update = $this->as->update_jenis_doc($data, $where);

            if($update){
               $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
               
            }else{
               $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
               $res_msg = $this->response_message->default_mgs($msg_main, null);
            }
            
        }
        
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_jenis_document--------------#
#=============================================================================# 

#=============================================================================#
#-------------------------------------------Index_pemeriksaan_wait------------#
#=============================================================================#
    public function index_pemeriksaan_wait(){
        $data["page"] = "super_doc_pemeriksaan_wait";
        $data["data_main"] = $this->adn->get_doc_where_fush(array("is_delete"=>"0", "sts_check"=>"0"));

        // print_r($data);

        $this->load->view('admin_super_new',$data);
    }

    public function get_data_for_check(){
        // print_r($_POST);
        $id = $this->input->post("id_doc");
        $data = $this->adn->get_doc_where_fush_each(array("id_doc"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function acc_doc(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("PENDAFTARAN_FAIL"));
        $id_doc = $this->input->post("id_doc");

        $id_admin_super = $this->session->userdata("admin_lv_1")["id_admin"];
        $time_check = date("Y-m-d h:i:s");

        $where  = array("id_doc"=>$id_doc);
        $set    = array("sts_check"=>"1",
                        "admin_check"=> $id_admin_super,
                        "time_check"=> $time_check);  

       
        $update_acc = $this->adn->update_doc($set, $where);
        if ($update_acc) {
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        }
                
        

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }

    public function remove_doc(){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("PENDAFTARAN_FAIL"));
        $id_doc = $this->input->post("id_doc");

        $id_admin_super = $this->session->userdata("admin_lv_1")["id_admin"];
        $time_check = date("Y-m-d h:i:s");

        $where  = array("id_doc"=>$id_doc);
        $set    = array("sts_check"=>"2",
                        "admin_check"=> $id_admin_super,
                        "time_check"=> $time_check);  

       
        $update_acc = $this->adn->update_doc($set, $where);
        if ($update_acc) {
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        }
                
        

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_pemeriksaan_wait------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------pemeriksaan_no--------------------#
#=============================================================================#
    public function pemeriksaan_no(){
        $data["page"] = "super_doc_pemeriksaan_no";
        $data["data_main"] = $this->adn->get_doc_where_fush(array("is_delete"=>"0", "sts_check!="=>"0"));

        // print_r($data);

        $this->load->view('admin_super_new',$data);
    }


#=============================================================================#
#-------------------------------------------pemeriksaan_no--------------------#
#=============================================================================#

}
