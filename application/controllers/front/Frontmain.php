<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('admin/admin_new', 'adn');
        $this->load->model('admin_super/super_admin', 'as');
    }

	public function index(){
		$this->load->view('front/header');
		$this->load->view('front/home_page');
		$this->load->view('front/footer');
	}

	public function var_kepatuhan(){
		$data["dinas"] = $this->as->get_dinas_active();

		$this->load->view('front/header');
		$this->load->view('front/variable_kepatuhan', $data);
		$this->load->view('front/footer');
	}

	public function get_data(){
		// print_r($_POST);
		$periode = $this->input->post("periode");
		$dinas 	 = $this->input->post("dinas");

		$where = array("dc.periode"=> $periode, "ad.id_bidang"=>$dinas);
		$data["all_doc"] = $this->adn->get_doc_where_fush($where);

		$this->load->view("front/variable_kepatuhan_list" ,$data);

		// print_r("<pre>");
		// print_r($data_dinas);
	}
}
