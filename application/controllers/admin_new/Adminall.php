<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminall extends CI_Controller{

	public function __construct(){
		parent::__construct();


		$this->load->model("admin/admin", "ad");
	}
    
    public function logout(){
        unset($_SESSION["admin_lv_1"]);
        redirect(base_url()."back-admin/login");
    }

    public function logout_super(){
        print_r($_SESSION);
        
    }
    
    public function surat_pengantar($id_pemohon){
        $data["pemohon"] = $this->ad->get_pemohon_surat(array("pe.id_pemohon"=>$id_pemohon));
        $data["anggota"] = $this->ad->get_anggota(array("id_pemohon"=>$id_pemohon));
        
        $dinas = $this->ad->get_dinas();
        foreach ($dinas as $val_dinas) {
            $data["dinas"][$val_dinas->id_dinas] = $val_dinas->nama_dinas;
        }
        
        //print_r("<pre>");
        //print_r($data);
        //$data["pemohon"] =
        $this->load->view("user_surat/v_RPP",$data); 
    }

    public function surat_pengantar_pkl($id_pemohon){
        $data["pemohon"] = $this->ad->get_pemohon_surat(array("pe.id_pemohon"=>$id_pemohon));
        $data["anggota"] = $this->ad->get_anggota(array("id_pemohon"=>$id_pemohon));
        
        $dinas = $this->ad->get_dinas();
        foreach ($dinas as $val_dinas) {
            $data["dinas"][$val_dinas->id_dinas] = $val_dinas->nama_dinas;
        }
        
        //print_r("<pre>");
        //print_r($data);
        //$data["pemohon"] =
        $this->load->view("user_surat/v_RPKL",$data); 
    }
    
    public function formulir_permohonan($id_pemohon){
        $data["pemohon"] = $this->ad->get_pemohon_surat(array("pe.id_pemohon"=>$id_pemohon));
        $data["anggota"] = $this->ad->get_anggota(array("id_pemohon"=>$id_pemohon));
        
        $dinas = $this->ad->get_dinas();
        foreach ($dinas as $val_dinas) {
            $data["dinas"][$val_dinas->id_dinas] = $val_dinas->nama_dinas;
        }
        
        //print_r("<pre>");
        //print_r($data);
        //$data["pemohon"] =
        $this->load->view("user_surat/v_formulir_permohonan",$data); 
    }
	
}
?>