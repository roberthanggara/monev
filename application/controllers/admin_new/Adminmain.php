<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmain extends CI_Controller {

    public function __construct(){
        parent::__construct(); 
        $this->load->model('admin/admin_new', 'adn');
        $this->load->model('admin_super/super_admin', 'as');

        $this->load->library("response_message");
        // $this->load->library("file_upload");
        
        $session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"] != 1 && $session["id_lv"] != 2 && $session["is_log"] != 1){
                redirect(base_url()."back-admin/login");
            }
        }else{
            redirect(base_url()."back-admin/login");
        }
    }

#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#
    private function main_upload_file($config, $input_name){
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $return_array = array("status"=>"",
                                "main_msg"=>"",
                                "main_data"=>"");
        
        if (!$this->upload->do_upload($input_name)){
            $return_array["status"] = false;
            $return_array["main_msg"] = array('error' => $this->upload->display_errors());
            $return_array["main_data"] = null;
        }else{
            $return_array["status"] = true;
            $return_array["main_msg"] = "upload success";
            $return_array["main_data"] = array('upload_data' => $this->upload->data());
        }

        return $return_array;
    }
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#


#=============================================================================#
#-------------------------------------------Index_Home------------------------#
#=============================================================================#
    public function index(){
        $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];

        $data["dinas"] = $this->as->get_dinas();
        $data["t_no_respons"]   = count($this->adn->get_doc_where(array("id_admin"=>$id_admin, "sts_check"=>"0")));
        $data["t_acc"]          = count($this->adn->get_doc_where(array("id_admin"=>$id_admin, "sts_check"=>"1")));
        $data["t_no_acc"]       = count($this->adn->get_doc_where(array("id_admin"=>$id_admin, "sts_check"=>"2")));

        $data["page"] = "dinas_home";

        $this->load->view('admin_main',$data);
    }
#=============================================================================#
#-------------------------------------------Index_Home------------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_document--------------------#
#=============================================================================#
    public function index_doc($periode){
        // print_r("<pre>");
        $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];
        $data_jenis_doc = $this->as->get_jenis_doc();

        $data["list_doc"] = array();
        foreach ($data_jenis_doc as $key => $value) {
            $data_doc = $this->adn->get_doc_where_each(array("periode"=> $periode, "id_admin"=>$id_admin, "id_jenis"=>$value->id_jenis));
            $data["list_doc"][$key]["jenis_doc"] = $value;
            $data["list_doc"][$key]["val_doc"] = $data_doc;
        }
        $data["page"] = "dinas_doc";

        // print_r($data);
        $this->load->view('admin_main',$data);
    }

    public function val_ins_doc(){
        $config_val_input = array(
                array(
                    'field'=>'id_jenis',
                    'label'=>'Jenis',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'periode',
                    'label'=>'periode',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }


    public function insert_doc(){
        // print_r($_POST);
        // print_r($_FILES);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_jenis"  =>"",
                    "periode"   =>"",
                    "url_upload"=>""
                );

        if($this->val_ins_doc()){
            $id_jenis = $this->input->post("id_jenis");
            $periode = $this->input->post("periode");

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_now = date("Y-m-d h:i:s");

            $cek_doc_where = array("id_jenis"=>$id_jenis, "periode"=>$periode, "id_admin"=> $id_admin);

            $cek_doc_periode = $this->adn->get_doc_where($cek_doc_where);
            if($cek_doc_periode == false){
                $data = array(
                                "id_doc"=>"",
                                "id_admin"=>$id_admin,
                                "id_jenis"=>$id_jenis,
                                "url_upload"=>"",
                                "periode"=>$periode,
                                "time_upload"=>$time_now,
                                "sts_check"=>"0",
                                "admin_check"=>"",
                                "time_check"=>"",

                                "is_delete"=>"0",
                                "admin_update"=>"",
                                "time_update"=>$time_now
                            );

                $insert = $this->adn->insert_doc($data);
                if($insert){
                    $select_id_ins = $this->adn->get_doc_where_each($cek_doc_where);
                    // print_r($select_id_ins["id_doc"]);

                    $config['upload_path']          = './assets/core_img/doc_img/';
                    $config['allowed_types']        = "pdf";
                    $config['max_size']             = 2048;
                    $config['file_name']            = hash("sha256", $select_id_ins["id_doc"]).".pdf";
                       
                    $upload_data = $this->main_upload_file($config, "file_unggah");

                    if($upload_data["status"]){
                        $where  = array("id_doc"      => $select_id_ins["id_doc"]);
                        $set    = array("url_upload"  => $upload_data["main_data"]["upload_data"]["file_name"]);

                        if($this->adn->update_doc($set, $where)){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                        }else {
                            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                        }
                    }else{
                        $detail_msg["url_upload"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                } 
            }           
        }else{
            $msg_detail["id_jenis"] = strip_tags(form_error('id_jenis'));
            $msg_detail["periode"] = strip_tags(form_error('periode'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        // print_r("<pre>");
        print_r(json_encode($res_msg));
        // print_r($cek_doc_periode);
    }

    public function get_doc(){
        // print_r($_POST);
        $id = $this->input->post("id_doc");
        $data = $this->adn->get_doc_where_each(array("id_doc"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = $data;
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function update_doc(){
        // print_r($_POST);
        // print_r($_FILES);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_jenis"  =>"",
                    "periode"   =>"",
                    "url_upload"=>""
                );

        if($this->val_ins_doc()){
            $id_jenis   = $this->input->post("id_jenis");
            $periode    = $this->input->post("periode");

            $id_doc     = $this->input->post("id_doc");  

            $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_now = date("Y-m-d h:i:s");

            $config['upload_path']          = './assets/core_img/doc_img/';
            $config['allowed_types']        = "pdf";
            $config['max_size']             = 2048;
            $config['file_name']            = hash("sha256", $id_doc).".pdf";
                       
            $upload_data = $this->main_upload_file($config, "file_unggah");

            if($upload_data["status"]){
                $where = array("id_doc"=>$id_doc);
                $data  = array("url_upload"=>hash("sha256", $id_doc).".pdf",
                                "admin_update"=>$id_admin,
                                "time_update"=>$time_now);

                $update = $this->adn->update_doc($data, $where);

                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }else {
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }else{
                    $detail_msg["url_upload"] = $upload_data["main_msg"]["error"];
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
            }
                      
        }else{
            $msg_detail["id_jenis"] = strip_tags(form_error('id_jenis'));
            $msg_detail["periode"] = strip_tags(form_error('periode'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        // print_r("<pre>");
        print_r(json_encode($res_msg));
        // print_r($cek_doc_periode);
    }

    public function delete_doc(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        
        $id_doc = $this->input->post("id_doc");

        $cek_doc_where = array("id_doc"=>$id_doc, "sts_check!="=>"1");

        $cek_doc_periode = $this->adn->get_doc_where($cek_doc_where);
        if($cek_doc_periode){
            // print_r("del");
            $delete_doc = $this->adn->delete_doc(array("id_doc"=>$id_doc));
            if ($delete_doc) {
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }            
        

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }
#=============================================================================#
#-------------------------------------------Index_document--------------------#
#=============================================================================#

#=============================================================================#
#-------------------------------------------Index_hasil-----------------------#
#=============================================================================#
    public function index_hasil($periode){
        $id_admin = $this->session->userdata("admin_lv_1")["id_admin"];
        $data_jenis_doc = $this->as->get_jenis_doc();

        $data["list_doc"] = array();
        foreach ($data_jenis_doc as $key => $value) {
            $data_doc = $this->adn->get_doc_where_each(array("periode"=> $periode, "id_admin"=>$id_admin, "id_jenis"=>$value->id_jenis));
            $data["list_doc"][$key]["jenis_doc"] = $value;
            $data["list_doc"][$key]["val_doc"] = $data_doc;
        }
        $data["page"] = "dinas_hasil";

        // print_r($data);
        $this->load->view('admin_main',$data);
    }
#=============================================================================#
#-------------------------------------------Index_hasil-----------------------#
#=============================================================================#


}
