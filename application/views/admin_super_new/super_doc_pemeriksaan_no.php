            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="row">
                    
                    <div class="col-12 m-t-30">
                        <!-- Card -->
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Tabel Data Dokumen yang Harus Diperiksa</h4>

                            </div>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive m-t-40">
                                            <table id="myTable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">No. </th>
                                                        <th width="20">Admin Dinas</th>
                                                        <th width="10">Tanggal Input</th>
                                                        <th width="15">Jenis Dokumen</th>
                                                        <th width="15">Dokumen</th>
                                                        <th width="5">Periode</th>
                                                        <th width="20%">Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if(!empty($data_main)){
                                                            $no = 1;
                                                            foreach ($data_main as $key => $value) {
                                                                $id_admin_add   = $value->id_admin;
                                                                $id_dinas       = $value->id_bidang;
                                                                $nama_dinas     = $value->nama_dinas;
                                                                $email_admin    = $value->email;

                                                                $time_upload    = $value->time_upload;
                                                                $jenis_doc      = $value->keterangan_jenis;
                                                                $dokumen        = $value->url_upload;
                                                                $periode        = $value->periode;

                                                                $sts_check        = $value->sts_check;
                                                                $time_check       = $value->time_check;

                                                                $str_cek = "Dokumen belum diperiksa";
                                                                if($sts_check == "1"){
                                                                    $str_cek = "Dokumen Telah dinyatakan diterima, pada tnggal : ".$time_check;
                                                                }elseif ($sts_check == "2") {
                                                                    $str_cek = "Dokumen Telah dinyatakan ditolak, pada tnggal : ".$time_check;
                                                                }
                                                                
                                                                print_r("<tr>
                                                                            <td>".$no."</td>
                                                                            <td>".$nama_dinas." (".$email_admin.")</td>
                                                                            <td>".$time_upload."</td>
                                                                            <td>".$jenis_doc."</td>
                                                                            <td>. . . . . <a href=\"".base_url()."assets/core_img/doc_img/".$dokumen."\" target=\"_blank\">".substr($dokumen, (strlen($dokumen)-30))."</a></td>
                                                                            <td>".$periode."</td>
                                                                            <td>".$str_cek."</td>
                                                                        </tr>");

                                                                $no++;
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>                     
                            </div>
                            <div class="card-body">
                               
                            </div>
                        </div>
                        <!-- Card -->
                    </div>
                </div>
                <!-- End Row -->                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<div class="modal fade" id="insert_jenis_dokumen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Jenis Dokumen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Keterangan Jenis Dokumen :</label>
                        <input type="text" class="form-control" id="keterangan_jenis_dokumen" name="keterangan_jenis_dokumen" required="">
                        <p id="msg_keterangan_jenis_dokumen" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Point Jenis Dokumen :</label>
                        <input type="number" step="any" max="100" min="0.1" class="form-control" id="point" name="point" required="">
                        <p id="msg_point" style="color: red;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="add_jenis_dokumen" type="submit" class="btn btn-primary">Simpan</button>
                </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<div class="modal fade" id="update_jenis_dokumen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah  Jenis Dokumen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Nama  Jenis Dokumen :</label>
                        <input type="text" class="form-control" id="keterangan_jenis_dokumen_up" name="keterangan_jenis_dokumen" required="">
                        <p id="_msg_keterangan_jenis_dokumen" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Point Jenis Dokumen :</label>
                        <input type="number" step="any" max="100" min="0.1" class="form-control" id="point_up" name="point" required="">
                        <p id="_msg_point" style="color: red;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="up_jenis_dokumen_btn" type="submit" class="btn btn-primary">Simpan</button>
                </div>
        </div>
    </div>
</div>
<!-- /.modal -->