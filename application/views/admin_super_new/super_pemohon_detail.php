<?php
    //echo "<pre>";
    $nama = "";
    $nik = "";
    $email = "";
    $alamat = "";
    $instansi = "";
    $tlp = "";
    $usia = "";
    $pekerjaan = "";
    $str_instansi = "";
    
    if(isset($user)){
        if(!empty($user)){
            
            $nama = $user["nama"];
            $nik = $user["nik"];
            $email = $user["email"];
            $alamat = $user["alamat"];
            $instansi = $user["instansi"];
            $tlp = $user["tlp"];
            
            $tgl = $user["tgl_lhr"];
            $usia = date("Y-m-d") - $tgl;
            
            $pekerjaan = $user["pekerjaan"];

            if(strpos($instansi, ";")){
                $tmp_ins = explode(";", $instansi);
                $str_instansi = "Jurusan : ".$tmp_ins[1].", Fakultas : ".$tmp_ins[2]. ", ". $tmp_ins[3];
            }else{
                $str_instansi = $instansi;
            }
                   
        }
    }
    
    $judul = "";
    $tmp_magang = "";
    $tgl_st = "";
    $tgl_fn = "";
    $anggota = "";
    $dosen = "";
    
    if(isset($pemohon_detail)){
        if(!empty($pemohon_detail)){
            $judul = $pemohon_detail["judul"];
            $tmp_magang = $pemohon_detail["id_bidang"];
            $tgl_st = $pemohon_detail["tgl_start"];
            $tgl_fn = $pemohon_detail["tgl_selesai"];
            $dosen = $pemohon_detail["dosen_pembimbing"];
        }
    }
    
    
    $id_doc = "";
    $url_ktp = "";
    $url_proposal = "";
    $url_sk = "";
    $no_surat = "";
    $nama_tdd = "";
    $jabatan = "";
    
    $avail_ktp = "Belum Terpenuhi";
    $avail_proposal = "Belum Terpenuhi";
    $avail_sk = "Belum Terpenuhi";
    
    if(isset($doc)){
        if(!empty($doc)){
            if($doc["ktp"]){
                $avail_ktp = "Terpenuhi";
            }
            
            if($doc["proposal"]){
                $avail_proposal = "Terpenuhi";
            }
            
            if($doc["sk"]){
                $avail_sk = "Terpenuhi";
            }
            
            $id_doc = $doc["doc"]["id_doc"];
            $url_ktp = $doc["doc"]["url_ktp"];
            $url_proposal = $doc["doc"]["url_proposal"];
            $url_sk = $doc["doc"]["url_sk"];
            $no_surat = $doc["doc"]["no_surat"];
            $nama_tdd = $doc["doc"]["nama_tdd"];
            $jabatan = $doc["doc"]["jabatan"];
        }
    }
    
    $jml_anggota = "";
    if(isset($t_angota)){
        if(!empty($t_angota)){
            $jml_anggota = $t_angota;
        }
    }    
?>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">


                                                <table width="100%" border="0">
                                                    <style type="text/css">
                                                        .row_tbl {
                                                              margin-bottom: 10px;
                                                        }
                                                    </style>
                                                    <tr>
                                                        <th colspan="3"><h1 class="page-title" align="left"> <i class="fe fe-credit-card"></i>&nbsp;Permohonan</h1></th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><hr></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">
                                                            <div class="row_tbl" >
                                                                <b>&nbsp;&nbsp;<i class="fe fe-user"></i> Nama</b>
                                                            </div>
                                                        </td>
                                                        <td align="center" width="5%">
                                                            <div class="row_tbl">
                                                                &nbsp;:&nbsp;
                                                            </div>
                                                        </td>
                                                        <td width="*">
                                                            <div class="row_tbl">
                                                                &nbsp;&nbsp;<?= $nama;?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="row_tbl">
                                                                <b>&nbsp;&nbsp;<i class="fe fe-map"></i> Alamat</b>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <div class="row_tbl">
                                                                &nbsp;:&nbsp;
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row_tbl">
                                                                &nbsp;&nbsp;<?= $alamat;?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="row_tbl"><b>&nbsp;&nbsp;<i class="fe fe-phone"></i> Telepon</b></div></td>
                                                        <td align="center"><div class="row_tbl">&nbsp;:&nbsp;</div></td>
                                                        <td><div class="row_tbl">&nbsp;&nbsp;<?= $tlp;?></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="row_tbl">
                                                                <b>&nbsp;&nbsp;<i class="fe fe-user"></i> Usia</b>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <div class="row_tbl">
                                                                &nbsp;:&nbsp;
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row_tbl">
                                                                &nbsp;&nbsp;<?= $usia;?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="row_tbl">
                                                                <b>&nbsp;&nbsp;<i class="fe fe-briefcase"></i> Pekerjaan</b>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <div class="row_tbl">
                                                                &nbsp;:&nbsp;
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row_tbl">
                                                                &nbsp;&nbsp;<?php if($pekerjaan == 0){ echo "Pekerja";}else{echo "Mahasiswa";}?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="row_tbl">
                                                                <b>&nbsp;&nbsp;<i class="fe fe-home"></i> Instansi</b>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <div class="row_tbl">
                                                                &nbsp;:&nbsp;
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row_tbl">
                                                                &nbsp;&nbsp;<?= $str_instansi;?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="row_tbl">
                                                                <b>&nbsp;&nbsp;<i class="fe fe-map-pin"></i> Lokasi Magang</b>
                                                            </div>
                                                        </td>
                                                        <td align="center">
                                                            <div class="row_tbl">
                                                                &nbsp;:&nbsp;
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row_tbl">
                                                                &nbsp;&nbsp;<?php 
                                                                    //$cek_tmp = str$tmp_magang;
                                                                        if(!empty($tmp_magang)){
                                                                            $tmp = strpos($tmp_magang, ";");
                                                                            $str_tmp_magang = "";
                                                                            if($tmp){
                                                                                $data_tmp_magang = explode(";",$tmp_magang);
                                                                                $no = 1;
                                                                                foreach($data_tmp_magang as $val_tmp){
                                                                                    $str_tmp_magang = $str_tmp_magang.$dinas[$val_tmp].", ";    
                                                                                }
                                                                            }else{
                                                                                $str_tmp_magang = $dinas[$tmp_magang];
                                                                            }
                                                                            echo $str_tmp_magang;
                                                                        }
                                                                            
                                                                        ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="row_tbl"><b>&nbsp;&nbsp;<i class="fe fe-clock"></i> Waktu Kegiatan</b></div></td>
                                                        <td align="center"><div class="row_tbl">&nbsp;:&nbsp;</div></td>
                                                        <td><div class="row_tbl">&nbsp;&nbsp;<?php echo $tgl_st." - ".$tgl_fn;?></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="row_tbl"><b>&nbsp;&nbsp;<i class="fe fe-monitor"></i> Judul</b></div></td>
                                                        <td align="center"><div class="row_tbl">&nbsp;:&nbsp;</div></td>
                                                        <td><div class="row_tbl">&nbsp;&nbsp;<?= $judul;?></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="row_tbl"><b>&nbsp;&nbsp;<i class="fe fe-users"></i> Jumlah Peserta</b></div></td>
                                                        <td align="center"><div class="row_tbl">&nbsp;:&nbsp;</div></td>
                                                        <td><div class="row_tbl">&nbsp;&nbsp;<?= $jml_anggota + 1;?></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="row_tbl"><b>&nbsp;&nbsp;<i class="fe fe-book-open"></i> Dosen</b></div></td>
                                                        <td align="center"><div class="row_tbl">&nbsp;:&nbsp;</div></td>
                                                        <td><div class="row_tbl">&nbsp;&nbsp;<?php
                                                                            $tmp = strpos($dosen, ";");
                                                                            $str_tmp_dosen = "";
                                                                            if($tmp){
                                                                                $data_tmp = explode(";",$dosen);
                                                                                echo implode(", ",$data_tmp);
                                                                            }else{
                                                                                echo $dosen;
                                                                            }
                                                                            
                                                                        ?></div></td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="3"><hr></td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3"><h1 class="page-title" align="left"> <i class="fe fe-credit-card"></i>&nbsp;Surat Pengantar</h1></th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><hr></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="row_tbl"><b>&nbsp;&nbsp;<i class="fe fe-book-open"></i> No. Surat Pernyataan</b></div></td>
                                                        <td align="center"><div class="row_tbl">&nbsp;:&nbsp;</div></td>
                                                        <td><div class="row_tbl">&nbsp;&nbsp;<?= $no_surat;?></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="row_tbl"><b>&nbsp;&nbsp;<i class="fe fe-users"></i> Yang Menandatangani</b></div></td>
                                                        <td align="center"><div class="row_tbl">&nbsp;:&nbsp;</div></td>
                                                        <td><div class="row_tbl">&nbsp;&nbsp;<?= $nama_tdd;?></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div class="row_tbl"><b>&nbsp;&nbsp;<i class="fe fe-feather"></i> Jabatan</b></div></td>
                                                        <td align="center"><div class="row_tbl">&nbsp;:&nbsp;</div></td>
                                                        <td><div class="row_tbl">&nbsp;&nbsp;<?= $jabatan;?></div></td>
                                                    </tr>
                                                </table>
                                            </div>
                                         </div>
                                      </div>
                                      <div class="col-lg-5">
                                         <div class="panel panel-default">
                                             <div class="panel-body">
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <th colspan="3"><h1 class="page-title" align="left"> <i class="fe fe-credit-card"></i>&nbsp;Dokumen</h1></th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><hr></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>&nbsp;&nbsp;<i class="fe fe-book-open"></i>&nbsp;Data Proposal</b></td>
                                                        <td align="center">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                                                        <td>&nbsp;&nbsp;<a href="<?=base_url()."doc/proposal/".$url_proposal;?>" style="width: 300px; height: 500px;" target="_blank">Proposal</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50%"><b>&nbsp;&nbsp;<i class="fe fe-monitor"></i>&nbsp;Data KTP</b></td>
                                                        <td width="5%" align="center">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                                                        <td width="*">&nbsp;&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3" align="center">
                                                            <a href="<?=base_url()."doc/ktp/".$url_ktp;?>" target="_blank">
                                                                <img src="<?=base_url()."doc/ktp/".$url_ktp;?>" style="width: 300px; height: 190px;">    
                                                            </a>
                                                            
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3" align="center"></th>
                                                    </tr>
                                                    <tr>
                                                        <td><b>&nbsp;&nbsp;<i class="fe fe-briefcase"></i>&nbsp;Data Surat Keterangan</b></td>
                                                        <td width="5%" align="center">&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                                                        <td>&nbsp;&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="3" align="center">
                                                            <a href="<?=base_url()."doc/sk/".$url_sk;?>" target="_blank">
                                                               <img src="<?=base_url()."doc/sk/".$url_sk;?>" style="width: 300px; height: 450px;">
                                                            </a>
                                                        </th>
                                                    </tr>
                                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>