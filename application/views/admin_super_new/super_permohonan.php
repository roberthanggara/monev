            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="row">
                    
                    <div class="col-12 m-t-30">
                        <!-- Card -->
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Tabel Data Jenis Dokumen</h4>

                            </div>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-body">
                                        <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_jenis_dokumen"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data Jenis Dokumen</button>

                                        <div class="table-responsive m-t-40">
                                            <table id="myTable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th width="15%">No. </th>
                                                        <th width="*">Keterangan Jenis Dokumen</th>
                                                        <th width="*">Point Dokumen</th>
                                                        <th width="20%">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if(!empty($jenis_dokumen)){
                                                            $max_point = 0;
                                                            foreach ($jenis_dokumen as $r_jenis_dokumen => $v_jenis_dokumen) {
                                                                $max_point += $v_jenis_dokumen->point;
                                                                echo "<tr>
                                                                        <td>".($r_jenis_dokumen+1)."</td>
                                                                        <td>".$v_jenis_dokumen->keterangan_jenis."</td>
                                                                        <td>".$v_jenis_dokumen->point."</td>
                                                                        <td>
                                                                        <center>
                                                                            <button type=\"button\" class=\"btn btn-info\" id=\"up_jenis_dokumen\" onclick=\"up_jenis_dokumen_fc('".$v_jenis_dokumen->id_jenis."');\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                            <button type=\"button\" class=\"btn btn-danger\" id=\"del_jenis_dokumen\" onclick=\"delete_jenis_dokumen('".$v_jenis_dokumen->id_jenis."');\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                        </center>
                                                                        </td>
                                                                    </tr>";
                                                            }
                                                        }
                                                    ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="2">Total Point</th>
                                                        <th colspan="2"><?php print_r($max_point);?></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>                     
                            </div>
                            <div class="card-body">
                                <div class="text-right">
                                    <label class="form-label">Keterangan Tombol Aksi ==> </label>

                                    <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                                    <label class="form-label text-info">Update Data</label>,&nbsp;
                                    <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                                    <label class="form-label text-danger">Delete Data</label>
                                </div>
                            </div>
                        </div>
                        <!-- Card -->
                    </div>
                </div>
                <!-- End Row -->                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<div class="modal fade" id="insert_jenis_dokumen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Jenis Dokumen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Keterangan Jenis Dokumen :</label>
                        <input type="text" class="form-control" id="keterangan_jenis_dokumen" name="keterangan_jenis_dokumen" required="">
                        <p id="msg_keterangan_jenis_dokumen" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Point Jenis Dokumen :</label>
                        <input type="number" step="any" max="100" min="0.1" class="form-control" id="point" name="point" required="">
                        <p id="msg_point" style="color: red;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="add_jenis_dokumen" type="submit" class="btn btn-primary">Simpan</button>
                </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<div class="modal fade" id="update_jenis_dokumen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah  Jenis Dokumen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Nama  Jenis Dokumen :</label>
                        <input type="text" class="form-control" id="keterangan_jenis_dokumen_up" name="keterangan_jenis_dokumen" required="">
                        <p id="_msg_keterangan_jenis_dokumen" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Point Jenis Dokumen :</label>
                        <input type="number" step="any" max="100" min="0.1" class="form-control" id="point_up" name="point" required="">
                        <p id="_msg_point" style="color: red;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="up_jenis_dokumen_btn" type="submit" class="btn btn-primary">Simpan</button>
                </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<script type="text/javascript">

    var id_jenis_gl;

//==========================================================================================//
//----------------------------------------------------Insert_jenis_dokumen------------------//
//==========================================================================================//
    $("#add_jenis_dokumen").click(function(){
        var data_main = new FormData();
        data_main.append('keterangan_jenis_dokumen', $("#keterangan_jenis_dokumen").val());
        data_main.append('point', $("#point").val());
        
        $.ajax({
            url: "<?php echo base_url()."admin_super/superadmin/insert_jenis_doc/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                // console.log(res);
                response_add_jenis_dokumen(res);
            }
        });
    });

    function response_add_jenis_dokumen(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
        console.log(data_json);
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data Jenis Dokumen berhasil disimpan ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."super/jenis_dokumen";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_keterangan_jenis_dokumen").html(detail_msg.keterangan_jenis_dokumen);
                        $("#msg_point").html(detail_msg.point);
                        
                        swal("Proses Gagal.!!", "Data Jenis Dokumen gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

                
            }
    }
//==========================================================================================//
//----------------------------------------------------Insert_jenis_dokumen------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Get_jenis_dokumen---------------------//
//==========================================================================================//
    function up_jenis_dokumen_fc(id_jenis) {
        clear_from_update();

        var data_main = new FormData();
        data_main.append('id_jenis', id_jenis);

        $.ajax({
            url: "<?php echo base_url()."admin_super/superadmin/get_jenis_doc_update/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                // console.log(res);
                set_val_update(res, id_jenis);
                $("#update_jenis_dokumen").modal('show');
            }
        });
    }

    function set_val_update(res, id_jenis) {
        var res_pemohon = JSON.parse(res);
        id_jenis_gl = id_jenis;

        if (res_pemohon.status == true) {
            $("#keterangan_jenis_dokumen_up").val(res_pemohon.val_response.keterangan_jenis);
            $("#point_up").val(res_pemohon.val_response.point);
        } else {
            clear_from_update();
        }
    }

    function clear_from_update() {
        $("#keterangan_jenis_dokumen_up").val("");
        $("#point_up").val("");
    }
//==========================================================================================//
//----------------------------------------------------Get_jenis_dokumen---------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Update_jenis_dokumen------------------//
//==========================================================================================//
    $("#up_jenis_dokumen_btn").click(function(){
        var data_main = new FormData();
        data_main.append('keterangan_jenis_dokumen', $("#keterangan_jenis_dokumen_up").val());
        data_main.append('point', $("#point_up").val());

        data_main.append('id_jenis', id_jenis_gl);

        $.ajax({
            url: "<?php echo base_url()."admin_super/superadmin/update_jenis_doc/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_update_jenis_dokumen(res);
            }
        });
    });

    function response_update_jenis_dokumen(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data Jenis Dokumen berhasil diubah ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."super/jenis_dokumen";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#_msg_keterangan_jenis_dokumen").html(detail_msg.keterangan_jenis_dokumen);
                        $("#_msg_point").html(detail_msg.keterangan_jenis_dokumen);
                        
                        swal("Proses Gagal.!!", "Data Jenis Dokumen gagal diubah, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

                
            }
    }
//==========================================================================================//
//----------------------------------------------------Update_jenis_dokumen------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Delete_jenis_dokumen------------------//
//==========================================================================================//
    function delete_jenis_dokumen(id_jenis){
        !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: false 
                    }, function(){
                        var data_main = new FormData();
                        data_main.append('id_jenis', id_jenis);

                        $.ajax({
                            url: "<?php echo base_url()."admin_super/superadmin/delete_jenis_doc/";?>", // point to server-side PHP script 
                            dataType: 'html', // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,
                            type: 'post',
                            success: function(res) {
                                console.log(res);
                                response_delete_jenis_dokumen(res);
                            }
                        });
                    });                              
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),
            
            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
    }

    function response_delete_jenis_dokumen(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                swal( {
                    title: "Proses Berhasil.!!",
                    text: "Data Jenis Dokumen berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href="<?php echo base_url()."super/jenis_dokumen";?>";
                });                   
            }else{
                swal("Proses Gagal.!!", "Data Jenis Dokumen gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");                             
            }
    }
//==========================================================================================//
//----------------------------------------------------Delete_jenis_dokumen------------------//
//==========================================================================================//
    

    
</script>
