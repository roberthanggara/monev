            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="row">
                    
                    <div class="col-12 m-t-30">
                        <!-- Card -->
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <h4 class="m-b-0 text-white">Tabel Data Dokumen yang Harus Diperiksa</h4>

                            </div>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive m-t-40">
                                            <table id="myTable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">No. </th>
                                                        <th width="20">Admin Dinas</th>
                                                        <th width="10">Tanggal Input</th>
                                                        <th width="15">Jenis Dokumen</th>
                                                        <th width="20">Dokumen</th>
                                                        <th width="10">Periode</th>
                                                        <th width="10%">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if(!empty($data_main)){
                                                            $no = 1;
                                                            foreach ($data_main as $key => $value) {
                                                                // print_r($value);
                                                                $id_admin_add   = $value->id_admin;
                                                                $id_dinas       = $value->id_bidang;
                                                                $nama_dinas     = $value->nama_dinas;
                                                                $email_admin    = $value->email;

                                                                $time_upload    = $value->time_upload;
                                                                $jenis_doc      = $value->keterangan_jenis;
                                                                $dokumen        = $value->url_upload;
                                                                $periode        = $value->periode;
                                                                $id_doc         = $value->id_doc;

                                                                $str_action     = "<button type=\"button\" class=\"btn btn-success\" id=\"upload_doc\" onclick=\"cek_btn('".$id_doc."')\" style=\"width: 40px;\">
                                                                                        <i class=\"fa fa-cloud-upload\"></i>
                                                                                    </button>";


                                                                
                                                                print_r("<tr>
                                                                            <td>".$no."</td>
                                                                            <td>".$nama_dinas." (".$email_admin.")</td>
                                                                            <td>".$time_upload."</td>
                                                                            <td>".$jenis_doc."</td>
                                                                            <td>. . . . . <a href=\"".base_url()."assets/core_img/doc_img/".$dokumen."\" target=\"_blank\">".substr($dokumen, (strlen($dokumen)-30))."</a></td>
                                                                            <td>".$periode."</td>
                                                                            <td>".$str_action."</td>
                                                                        </tr>");

                                                                $no++;
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th width="5%">No. </th>
                                                        <th width="20">Admin Dinas</th>
                                                        <th width="10">Tanggal Input</th>
                                                        <th width="15">Jenis Dokumen</th>
                                                        <th width="20">Dokumen</th>
                                                        <th width="10">Periode</th>
                                                        <th width="10%">Aksi</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>                     
                            </div>
                            <div class="card-body">
                                <div class="text-right">
                                    <label class="form-label">Keterangan Tombol Aksi ==> </label>

                                    <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                                    <label class="form-label text-info">Update Data</label>,&nbsp;
                                    <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                                    <label class="form-label text-danger">Delete Data</label>
                                </div>
                            </div>
                        </div>
                        <!-- Card -->
                    </div>
                </div>
                <!-- End Row -->                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<div class="modal fade" id="cek_dok_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Jenis Dokumen</h4><br>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label"><h4><b>Dinas Pengirim : </b></h4></label><br>
                        <p id="dinas"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label"><h4><b>Jenis Dokumen : </b></h4></label><br>
                        <p id="jenis_doc"></p>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label"><h4><b>Dokumen : </b></h4></label><br>
                        <a id="doc" href=""></a>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label"><h4><b>Keterangan Dokumen : </b></h4></label><br>
                        <p id="keterangan_doc"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="add_terima" type="submit" class="btn btn-info">Terima Dokumen</button>
                    <button id="add_tolak" type="submit" class="btn btn-danger">Tolak Dokumen</button>
                </div>
        </div>
    </div>
</div>
<!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function(){

    });

    function cek_btn(id_doc){
        var data_main = new FormData();
        data_main.append('id_doc', id_doc);
        // console.log("ok");

        $.ajax({

            url: "<?php echo base_url()."admin_super/Superadmin/get_data_for_check";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_update_dinas(res);
            }
        });
    }

    var id_doc_gl;

    function response_update_dinas(res){
        clear_cek_modal();
        var data_json = JSON.parse(res);
                var sts = data_json.status;
                var val_response = data_json.val_response;

                console.log(val_response);
            if(sts){
                var nama_dinas  = val_response.nama_dinas;
                var user        = val_response.email;

                var jenis_doc   = val_response.keterangan_jenis;
                var doc         = val_response.url_upload;

                var time_upload  = val_response.time_upload;

                var pdf_str_length = doc.length;

                $("#doc").html(" . . . . . "+doc.substr(pdf_str_length-30, pdf_str_length));
                $("#doc").attr("href", "<?php print_r(base_url()."assets/core_img/doc_img/");?>"+doc);

                $("#dinas").html(nama_dinas+" ("+user+")");
                $("#jenis_doc").html(jenis_doc);
                $("#keterangan_doc").html("Berkas di unggah pada "+time_upload);

                $("#cek_dok_modal").modal("show");

                id_doc_gl = val_response.id_doc;
            }else{
                clear_cek_modal();
            }
    }

    function clear_cek_modal(){
        $("#doc").html("");
        $("#doc").attr("href", "#");

        $("#dinas").html("");
        $("#jenis_doc").html("");
        $("#keterangan_doc").html("");
    }

    $("#add_terima").click(function(){
        var data_main = new FormData();
        data_main.append('id_doc', id_doc_gl);
        // console.log("ok");

        $.ajax({

            url: "<?php echo base_url()."admin_super/Superadmin/acc_doc";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_doc_process(res);
            }
        });
    });

    $("#add_tolak").click(function(){
        var data_main = new FormData();
        data_main.append('id_doc', id_doc_gl);
        // console.log("ok");

        $.ajax({

            url: "<?php echo base_url()."admin_super/Superadmin/remove_doc";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_doc_process(res);
            }
        });
    });

     function response_doc_process(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data Dokumen berhasil dieksekusi ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."super/pemeriksaan_wait";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        
                        swal("Proses Gagal.!!", "Data Dinas gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

                
            }
    }


</script>