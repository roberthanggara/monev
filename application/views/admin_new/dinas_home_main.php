            <?php
                $str_t_no_respons = "0";
                if($t_no_respons){
                    $str_t_no_respons = $t_no_respons;
                }


                $str_t_acc = "0";
                if($t_acc){
                    $str_t_acc = $t_acc;
                }


                $str_t_no_acc = "0";
                if($t_no_acc){
                    $str_t_no_acc = $t_no_acc;
                }
            ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- Row -->
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-file-import text-info"></i></h2>
                                    <h3 class=""><?= $str_t_no_respons; ?></h3>
                                    <h6 class="card-subtitle">Total Dokumen Belum Direspons</h6></div>
                                <div class="col-12">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-file-multiple text-success"></i></h2>
                                    <h3 class=""><?= $str_t_acc; ?></h3>
                                    <h6 class="card-subtitle">Total Dokumen Diterima</h6></div>
                                <div class="col-12">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="m-b-0"><i class="mdi mdi-file-excel text-danger"></i></h2>
                                    <h3 class=""><?= $str_t_no_acc; ?></h3>
                                    <h6 class="card-subtitle">Total Dokumen Ditolak</h6></div>
                                <div class="col-12">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    
                </div>
                           
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

            <script src="<?php print_r(base_url());?>admin_template/assets/plugins/echarts/echarts-all.js"></script>
            <script type="text/javascript">
                // ============================================================== 
                // Bar chart option
                // ============================================================== 
                var myChart = echarts.init(document.getElementById('bar-chart'));

                // specify chart configuration item and data
                option = {
                    tooltip : {
                        trigger: 'axis'
                    },
                    legend: {
                        data:['Magang','Penelitian']
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            
                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    color: ["#55ce63", "#009efb"],
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            data : ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        {
                            name:'Magang',
                            type:'bar',
                            data:[<?php
                                    if(is_array($pemohon_list_m)){print_r(implode(",", $pemohon_list_m));}
                                ?>],
                            
                        },
                        {
                            name:'Penelitian',
                            type:'bar',
                            data:[<?php
                                    if(is_array($pemohon_list_p)){print_r(implode(",", $pemohon_list_p));}
                                ?>],
                            
                        }
                    ]
                };


                // use configuration item and data specified to show chart
                myChart.setOption(option, true), $(function() {
                            function resize() {
                                setTimeout(function() {
                                    myChart.resize()
                                }, 100)
                            }
                            $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
                        });
                     
            </script>