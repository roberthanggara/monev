            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- <embed id="view_upload" src="http://localhost/monev/assets/core_img/doc_img/f5ca38f748a1d6eaf726b8a42fb575c3c71f1864a8143301782de13da2d9202b.pdf#toolbar=0&amp;navpanes=0&amp;scrollbar=0" type="application/pdf" width="100%" height="600px" internalinstanceid="37"> -->
                <!-- Row -->
                <div class="row">
                    
                    <div class="col-12 m-t-30">
                        <!-- Card -->
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-lg-9"><b><h4 class="m-b-0 text-white">Tabel Data Upload Dokumen</h4></b></div>
                                    <div class="col-lg-3 text-right">
                                        <select id="periode" name="periode" class="form-control">
                                            <?php
                                                $margin = 50;
                                                $th_st = date("Y") - $margin;
                                                $th_fn = date("Y") + $margin;

                                                $th_now = date("Y");
                                                
                                                for ($i=$th_st; $i < $th_fn; $i++) { 
                                                    print_r("<option value=\"".$i."\">".$i."</option>");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive m-t-40">
                                            <table id="myTable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">No. </th>
                                                        <th width="20%">Jenis Dokumen</th>
                                                        <th width="10%">Periode Penilaian</th>
                                                        <th width="20%">Dokumen</th>
                                                        <th width="25%">Keterangan</th>
                                                        <th width="15%">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if(!empty($list_doc)){
                                                            // print_r("<pre>");
                                                            // print_r($list_doc);
                                                            $no = 1;
                                                            foreach ($list_doc as $key => $value) {
                                                                // print_r("<pre>");
                                                                // print_r($value["jenis_doc"]->id_jenis);
                                                                $jenis_doc  = $value["jenis_doc"]->keterangan_jenis;
                                                                $id_jenis   = $value["jenis_doc"]->id_jenis;
                                                                $point      = $value["jenis_doc"]->point;

                                                                $id_doc     = "";
                                                                $id_admin   = "";
                                                                $url_upload = "";
                                                                $periode    = "";
                                                                $time_upload    = "";
                                                                $sts_check      = "";
                                                                $admin_check    = "";
                                                                $time_check     = "";

                                                                $str_status_cek = "Dokumen Belum di Unggah";
                                                                $str_action     = "<button type=\"button\" class=\"btn btn-success\" id=\"upload_doc\" onclick=\"upload_doc_f('".$id_jenis."')\" style=\"width: 40px;\">
                                                                                        <i class=\"fa fa-cloud-upload\"></i>
                                                                                    </button>";

                                                                if($value["val_doc"]){
                                                                    $id_doc     = $value["val_doc"]["id_doc"];
                                                                    $id_jenis   = $value["val_doc"]["id_jenis"];
                                                                    $id_admin   = $value["val_doc"]["id_admin"];
                                                                    $url_upload = $value["val_doc"]["url_upload"];
                                                                    $periode    = $value["val_doc"]["periode"];
                                                                    $time_upload    = $value["val_doc"]["time_upload"];
                                                                    $sts_check      = $value["val_doc"]["sts_check"];
                                                                    $admin_check    = $value["val_doc"]["admin_check"];
                                                                    $time_check     = $value["val_doc"]["time_check"];

                                                                    if($sts_check == "1"){
                                                                        $str_status_cek = "Dokumen Sudah Di Periksa dan Dinyatakan Memenuhi Syarat Oleh admin, Pada tanggal: ".$time_upload;
                                                                        $str_action     = "Dokumen sudah dinyatakan final";

                                                                    }elseif ($sts_check == "2") {
                                                                        $str_status_cek = "Dokumen Ditolak karena Tidak Memenuhi Standart Penilaian Oleh admin";

                                                                        $str_action     = "<button type=\"button\" class=\"btn btn-info\" id=\"up_dinas\" onclick=\"up_dinas_fc('".$id_jenis."', '".$id_doc."')\" style=\"width: 40px;\">
                                                                                        <i class=\"fa fa-pencil-square-o\"></i>
                                                                                    </button>
                                                                                    &nbsp;&nbsp;
                                                                                    <button type=\"button\" class=\"btn btn-danger\" id=\"del_dinas\" onclick=\"delete_dinas('".$id_doc."')\" style=\"width: 40px;\">
                                                                                        <i class=\"fa fa-trash-o\"></i>
                                                                                    </button>";
                                                                    }elseif ($sts_check == "0") {
                                                                        $str_status_cek = "Dokumen Belum di Proses Oleh Admin";

                                                                        $str_action     = "<button type=\"button\" class=\"btn btn-info\" id=\"up_dinas\" onclick=\"up_dinas_fc('".$id_jenis."', '".$id_doc."')\" style=\"width: 40px;\">
                                                                                        <i class=\"fa fa-pencil-square-o\"></i>
                                                                                    </button>
                                                                                    &nbsp;&nbsp;
                                                                                    <button type=\"button\" class=\"btn btn-danger\" id=\"del_dinas\" onclick=\"delete_dinas('".$id_doc."')\" style=\"width: 40px;\">
                                                                                        <i class=\"fa fa-trash-o\"></i>
                                                                                    </button>";
                                                                    }
                                                                }
                                                                
                                                                echo "<tr>
                                                                        <td>".$no."</td>
                                                                        <td>".$jenis_doc."</td>
                                                                        <td>".$periode."</td>
                                                                        <td>. . . . . <a href=\"".base_url()."assets/core_img/doc_img/".$url_upload."\" target=\"_blank\">".substr($url_upload, (strlen($url_upload)-30))."</a></td>
                                                                        <td>".$str_status_cek."</td>
                                                                                                                                                
                                                                        <td>
                                                                        <center>
                                                                            ".$str_action."
                                                                        </center>
                                                                        </td>
                                                                    </tr>";
                                                                    $no++;
                                                            }
                                                        }
                                                    ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>                     
                            </div>
                            <div class="card-body">
                                <div class="text-right">
                                    <label class="form-label">Keterangan Tombol Aksi ==> </label>

                                    <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                                    <label class="form-label text-info">Update Data</label>,&nbsp;
                                    <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                                    <label class="form-label text-danger">Delete Data</label>
                                </div>
                            </div>
                        </div>
                        <!-- Card -->
                    </div>
                </div>
                <!-- End Row -->                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->

<div class="modal fade" id="insert_dinas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Dinas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">File yang diunggah :</label>
                        <input type="File" class="form-control" id="file_unggah" name="file_unggah" required="">
                        <p id="msg_file_unggah" style="color: red;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="add_dinas" type="submit" class="btn btn-primary">Simpan</button>
                </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<div class="modal fade" id="update_dinas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Dinas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">File yang diunggah :</label>
                        <input type="File" class="form-control" id="file_unggah_up" name="file_unggah" required="">
                        <p id="msg_file_unggah_up" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="href_upload" class="control-label">File pdf : </label>
                        <a id="href_upload" href=""></a>
                        <!-- <embed id="view_upload" src="" type="application/pdf" width="100%" height="600px" internalinstanceid="21"> -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="up_dinas_btn" type="submit" class="btn btn-primary">Simpan</button>
                </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<script type="text/javascript">

    $(document).ready(function(){
        $("#periode").val("<?php print_r($this->uri->segment(3));?>");
    });

//==========================================================================================//
//----------------------------------------------------Insert_Dinas--------------------------//
//==========================================================================================//

    var id_jenis_ins = "";
    var id_doc_ins = "";
    var periode_ins = "<?php print_r($this->uri->segment(3));?>";

    var file = [];
    $("#file_unggah").change(function(e){
        file = e.target.files[0];
        console.log(file);
    });  

    function upload_doc_f(id_jenis){
        $("#insert_dinas").modal("show");
        id_jenis_ins = id_jenis;
    }

    $("#add_dinas").click(function(){
        var data_main = new FormData();
        data_main.append('file_unggah', file);
        data_main.append('id_jenis' , id_jenis_ins);
        data_main.append('periode'  , periode_ins);

        $.ajax({
            url: "<?php echo base_url()."admin_new/adminmain/insert_doc/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_add_dinas(res);
            }
        });
    });

    function response_add_dinas(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data Dinas berhasil disimpan ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."dinas/dokumen/";?>"+periode_ins;
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_file_unggah").html(detail_msg.url_upload);
                        
                        swal("Proses Gagal.!!", "Data Dinas gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

                
            }
    }
//==========================================================================================//
//----------------------------------------------------Insert_Dinas--------------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Get_Dinas-----------------------------//
//==========================================================================================//
    var id_doc_gl;
    var id_jenis_gl;

    function up_dinas_fc(id_jenis, id_doc) {
        clear_from_update();

        var data_main = new FormData();
        data_main.append('id_doc', id_doc);

        $.ajax({
            url: "<?php echo base_url()."admin_new/adminmain/get_doc/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                set_val_update(res, id_doc, id_jenis);
                $("#update_dinas").modal('show');
            }
        });
    }

    function set_val_update(res, id_doc, id_jenis) {
        var res_pemohon = JSON.parse(res);
        id_doc_gl   = id_doc;
        id_jenis_gl = id_jenis;

        if (res_pemohon.status == true) {
            // $("#file_unggah_up").val(res_pemohon.val_response.url_upload);
            $("#href_upload").attr("href", "<?php print_r(base_url()."assets/core_img/doc_img/");?>"+res_pemohon.val_response.url_upload);
            var pdf_str_length = res_pemohon.val_response.url_upload.length;

            $("#href_upload").html(". . . . . "+res_pemohon.val_response.url_upload.substr(pdf_str_length-30, pdf_str_length));
            // $("#view_upload").attr("src", "<?php print_r(base_url()."assets/core_img/doc_img/");?>"+res_pemohon.val_response.url_upload+"#toolbar=0&navpanes=0&scrollbar=0");
        } else {
            clear_from_update();
        }
    }

    function clear_from_update() {
        $("#file_unggah_up").val("");
    }
//==========================================================================================//
//----------------------------------------------------Get_Dinas-----------------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Update_Dinas--------------------------//
//==========================================================================================//

    var file = [];
        $("#file_unggah_up").change(function(e){
            file = e.target.files[0];
            console.log(file);
    });  

    $("#up_dinas_btn").click(function(){
        var data_main = new FormData();
        data_main.append('file_unggah', file);
        data_main.append('id_doc', id_doc_gl);
        data_main.append('id_jenis', id_jenis_gl);
        data_main.append('periode', periode_ins);

        $.ajax({
            url: "<?php echo base_url()."admin_new/adminmain/update_doc/";?>", // point to server-side PHP script 
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_update_dinas(res);
            }
        });
    });

    function response_update_dinas(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data Dinas berhasil diubah ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."dinas/dokumen/";?>"+periode_ins;
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_file_unggah_up").html(detail_msg.url_upload);
                        
                        swal("Proses Gagal.!!", "Data Dinas gagal diubah, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

                
            }
    }
//==========================================================================================//
//----------------------------------------------------Update_Dinas--------------------------//
//==========================================================================================//

//==========================================================================================//
//----------------------------------------------------Delete_Dinas--------------------------//
//==========================================================================================//
    function delete_dinas(id_doc){
        !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: false 
                    }, function(){
                        var data_main = new FormData();
                        data_main.append('id_doc', id_doc);

                        $.ajax({
                            url: "<?php echo base_url()."admin_new/adminmain/delete_doc/";?>", // point to server-side PHP script 
                            dataType: 'html', // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,
                            type: 'post',
                            success: function(res) {
                                console.log(res);
                                response_delete_dinas(res);
                            }
                        });
                    });                              
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),
            
            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
    }

    function response_delete_dinas(res){
        var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                swal( {
                    title: "Proses Berhasil.!!",
                    text: "Data Dinas berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."dinas/dokumen/";?>"+periode_ins;
                });                   
            }else{
                
                swal("Proses Gagal.!!", "Data Dinas gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");                                 
            }
    }
//==========================================================================================//
//----------------------------------------------------Delete_Dinas--------------------------//
//==========================================================================================//


$("#periode").change(function(){
    console.log($("#periode").val());
    window.location.href = "<?php print_r(base_url()."dinas/dokumen/")?>"+ $("#periode").val();
});
    

    
</script>
