            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- <embed id="view_upload" src="http://localhost/monev/assets/core_img/doc_img/f5ca38f748a1d6eaf726b8a42fb575c3c71f1864a8143301782de13da2d9202b.pdf#toolbar=0&amp;navpanes=0&amp;scrollbar=0" type="application/pdf" width="100%" height="600px" internalinstanceid="37"> -->
                <!-- Row -->
                <div class="row">
                    
                    <div class="col-12 m-t-30">
                        <!-- Card -->
                        <div class="card card-outline-info">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-lg-9"><b><h4 class="m-b-0 text-white">Tabel Data Upload Dokumen</h4></b></div>
                                    <div class="col-lg-3 text-right">
                                        <select id="periode" name="periode" class="form-control">
                                            <?php
                                                $margin = 50;
                                                $th_st = date("Y") - $margin;
                                                $th_fn = date("Y") + $margin;

                                                $th_now = date("Y");
                                                
                                                for ($i=$th_st; $i < $th_fn; $i++) { 
                                                    print_r("<option value=\"".$i."\">".$i."</option>");
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive m-t-40">
                                            <table id="myTable" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">No. </th>
                                                        <th width="20%">Jenis Dokumen</th>
                                                        <th width="10%">Periode Penilaian</th>
                                                        <th width="20%">Dokumen</th>
                                                        <th width="25%">Keterangan</th>
                                                        <th width="15%">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if(!empty($list_doc)){
                                                            // print_r("<pre>");
                                                            // print_r($list_doc);
                                                            $no = 1;

                                                            $count_point = 0;
                                                            $max_point = 0;
                                                            foreach ($list_doc as $key => $value) {
                                                                // print_r("<pre>");
                                                                // print_r($value["jenis_doc"]->id_jenis);
                                                                $jenis_doc  = $value["jenis_doc"]->keterangan_jenis;
                                                                $id_jenis  = $value["jenis_doc"]->id_jenis;
                                                                $point      = $value["jenis_doc"]->point;
                                                                $max_point += $point;

                                                                $id_doc     = "";
                                                                $id_admin   = "";
                                                                $url_upload = "";
                                                                $periode    = "";
                                                                $time_upload    = "";
                                                                $sts_check      = "";
                                                                $admin_check    = "";
                                                                $time_check     = "";

                                                                $str_status_cek = "Dokumen Belum di Unggah";
                                                                $str_action     = 0;

                                                                if($value["val_doc"]){
                                                                    $id_doc     = $value["val_doc"]["id_doc"];
                                                                    $id_jenis   = $value["val_doc"]["id_jenis"];
                                                                    $id_admin   = $value["val_doc"]["id_admin"];
                                                                    $url_upload = $value["val_doc"]["url_upload"];
                                                                    $periode    = $value["val_doc"]["periode"];
                                                                    $time_upload    = $value["val_doc"]["time_upload"];
                                                                    $sts_check      = $value["val_doc"]["sts_check"];
                                                                    $admin_check    = $value["val_doc"]["admin_check"];
                                                                    $time_check     = $value["val_doc"]["time_check"];

                                                                    if($sts_check == "1"){
                                                                        $str_status_cek = "Dokumen Sudah Di Periksa dan Dinyatakan Memenuhi Syarat Oleh admin, Pada tanggal: ".$time_upload;
                                                                        $str_action     = $point;

                                                                    }elseif ($sts_check == "2") {
                                                                        $str_status_cek = "Dokumen Ditolak karena Tidak Memenuhi Standart Penilaian Oleh admin";

                                                                        $str_action     = 0;
                                                                    }elseif ($sts_check == "0") {
                                                                        $str_status_cek = "Dokumen Belum di Proses Oleh Admin";

                                                                        $str_action     = 0;
                                                                    }
                                                                }

                                                                $count_point += $str_action;

                                                                
                                                                echo "<tr>
                                                                        <td>".$no."</td>
                                                                        <td>".$jenis_doc."</td>
                                                                        <td>".$periode."</td>
                                                                        <td>. . . . . <a href=\"".base_url()."assets/core_img/doc_img/".$url_upload."\" target=\"_blank\">".substr($url_upload, (strlen($url_upload)-30))."</a></td>
                                                                        <td>".$str_status_cek."</td>
                                                                                                                                                
                                                                        <td>
                                                                        <center>
                                                                            ".$str_action."
                                                                        </center>
                                                                        </td>
                                                                    </tr>";
                                                                    $no++;
                                                            }
                                                        }
                                                    ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="5">Total Nilai </th>
                                                        <th width="15%"><?php print_r(number_format($count_point, 2, ".", ","). "  (".$max_point.")");?></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>                     
                            </div>
                            
                        </div>
                        <!-- Card -->
                    </div>
                </div>
                <!-- End Row -->                
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->


<script type="text/javascript">

    $(document).ready(function(){
        $("#periode").val("<?php print_r($this->uri->segment(3));?>");
    });


    $("#periode").change(function(){
        console.log($("#periode").val());
        window.location.href = "<?php print_r(base_url()."dinas/hasil_penilaian/")?>"+ $("#periode").val();
    });

    
</script>
