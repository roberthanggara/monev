
    <header id="header">
        <div class="intro">
            <!--  <div class="overlay"> -->
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 intro-text">
                        <h1>E-MONEV ORGANISASI</h1>
                        <h2 style="color: #ffffff;">STANDAR PELAYANAN PUBLIK</h2>
                        <h2 style="color: #ffffff;">PEMERINTAH KOTA BATU</h2>
                </div>
            </div>
        </div>
        </div>
    </header>
    <!-- About Section -->
    <div id="about">
        <div class="container">
            <div class="row">
                <center><h3><b>E-MONEV ORGANISASI</b></h3></center>

                <center><h3>SISTEM MONITORING DAN EVALUASI KEPATUHAN TERHADAP STANDAR PELAYANAN PUBLIK PEMERINTAH KOTA BATU</h3></center>
                <br><br>
                <h5><b>STANDAR PELAYANAN PUBLIK BERDASARKAN UNDANG-UNDANG NOMOR 25 TAHUN 2009 TENTANG PELAYANAN PUBLIK :</b></h5>
                <table width="100%" border="0">
                    <tr>
                        <td width="5%" valign="top"><h5>1. </h5></td>
                        <td width="95%" valign="top"><h5>
                            Pelayanan publik adalah kegiatan atau rangkaian kegiatan dalam rangka pemenuhan kebutuhan pelayanan sesuai dengan peraturan perundang-undangan bagi setiap warga negara dan penduduk atas barang, jasa, dan/atau pelayanan administratif yang disediakan oleh penyelenggara pelayanan publik.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>2. </h5></td>
                        <td width="95%" valign="top"><h5>
                            Penyelenggara pelayanan publik yang selanjutnya disebut Penyelenggara adalah setiap institusi penyelenggara negara, korporasi, lembaga independen yang dibentuk berdasarkan undang-undang untuk kegiatan pelayanan publik, dan badan hukum lain yang dibentuk semata-mata untuk kegiatan pelayanan publik.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>3. </h5></td>
                        <td width="95%" valign="top"><h5>
                            Atasan satuan kerja Penyelenggara adalah pimpinan satuan kerja yang membawahi secara langsung satu atau lebih satuan kerja yang melaksanakan pelayanan publik.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>4. </h5></td>
                        <td width="95%" valign="top"><h5>
                            Organisasi penyelenggara pelayanan publik yang selanjutnya disebut Organisasi Penyelenggara adalah satuan kerja penyelenggara pelayanan publik yang berada di lingkungan institusi penyelenggara negara, korporasi, lembaga independen yang dibentuk berdasarkan undang-undang untuk kegiatan pelayanan publik, dan badan hukum lain yang dibentuk semata-mata untuk kegiatan pelayanan publik.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>5. </h5></td>
                        <td width="95%" valign="top"><h5>
                            Pelaksana pelayanan publik yang selanjutnya disebut Pelaksana adalah pejabat, pegawai, petugas, dan setiap orang yang bekerja di dalam Organisasi Penyelenggara yang bertugas melaksanakan tindakan atau serangkaian tindakan pelayanan publik.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>6. </h5></td>
                        <td width="95%" valign="top"><h5>
                            Masyarakat adalah seluruh pihak, baik warga negara maupun penduduk sebagai orang-perseorangan, kelompok, maupun badan hukum yang berkedudukan sebagai penerima manfaat pelayanan publik, baik secara langsung maupun tidak langsung.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>7. </h5></td>
                        <td width="95%" valign="top"><h5>
                            Standar pelayanan adalah tolok ukur yang dipergunakan sebagai pedoman penyelenggaraan pelayanan dan acuan penilaian kualitas pelayanan sebagai kewajiban dan janji Penyelenggara kepada masyarakat dalam rangka pelayanan yang berkualitas, cepat, mudah, terjangkau, dan terukur.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>8. </h5></td>
                        <td width="95%" valign="top"><h5>
                            Maklumat pelayanan adalah pernyataan tertulis yang berisi keseluruhan rincian kewajiban dan janji yang terdapat dalam standar pelayanan.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>9. </h5></td>
                        <td width="95%" valign="top"><h5>
                             Sistem informasi pelayanan publik yang selanjutnya disebut Sistem Informasi adalah rangkaian kegiatan yang meliputi penyimpanan dan pengelolaan informasi serta mekanisme penyampaian informasi dari Penyelenggara kepada masyarakat dan sebaliknya dalam bentuk lisan, tulisan Latin, tulisan dalam huruf Braile, bahasa gambar, dan/atau bahasa lokal, serta disajikan secara manual ataupun elektronik.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>10. </h5></td>
                        <td width="95%" valign="top"><h5>
                             Mediasi adalah penyelesaian sengketa pelayanan publik antarpara pihak melalui bantuan, baik oleh ombudsman sendiri maupun melalui mediator yang dibentuk oleh ombudsman.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>11. </h5></td>
                        <td width="95%" valign="top"><h5>
                             Ajudikasi adalah proses penyelesaian sengketa pelayanan publik antarpara pihak yang diputus oleh ombudsman.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>12. </h5></td>
                        <td width="95%" valign="top"><h5>
                             Menteri adalah menteri yang bertanggung jawab di bidang pendayagunaan aparatur negara.
                        </h5></td>
                    </tr>

                    <tr>
                        <td width="5%" valign="top"><h5>13. </h5></td>
                        <td width="95%" valign="top"><h5>
                             Ombudsman adalah lembaga negara yang mempunyai kewenangan mengawasi penyelenggaraan pelayanan publik, baik yang diselenggarakan oleh penyelenggara negara dan pemerintahan termasuk yang diselenggarakan oleh badan usaha milik negara, badan usaha milik daerah, dan badan hukum milik negara serta badan swasta, maupun perseorangan yang diberi tugas menyelenggarakan pelayanan publik tertentu yang sebagian atau seluruh dananya bersumber dari anggaran pendapatan dan belanja negara dan/atau anggaran pendapatan dan belanja daerah.
                        </h5></td>
                    </tr>
                    
                </table>

            </div>
        </div>
    </div>
    <!-- Services Section -->

    