
    <header id="header">
        <div class="intro">
            <!--  <div class="overlay"> -->
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 intro-text">
                        <h1>E-MONEV ORGANISASI</h1>
                        <h2 style="color: #ffffff;">STANDAR PELAYANAN PUBLIK</h2>
                        <h2 style="color: #ffffff;">PEMERINTAH KOTA BATU</h2>
                </div>
            </div>
        </div>
        </div>
    </header>
    <!-- About Section -->
    <div id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4">
                            <select id="periode" name="periode" class="form-control">
                                <?php
                                    $margin = 50;
                                    $th_st = date("Y") - $margin;
                                    $th_fn = date("Y") + $margin;

                                    $th_now = date("Y");
                                                
                                    for ($i=$th_st; $i < $th_fn; $i++) { 
                                        print_r("<option value=\"".$i."\">".$i."</option>");
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-8">
                            <select id="dinas" name="dinas" class="form-control">
                                <?php
                                    if($dinas){
                                        foreach ($dinas as $key => $value) {
                                            print_r("<option value=\"".$value->id_dinas."\">".$value->nama_dinas."</option>");
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    
                    <br><br>
                
                </div>
                <div class="col-lg-12" id="out_table">
                                        
                </div>               
            </div>
        </div>
    </div>
    <!-- Services Section -->

    <script src="<?php print_r(base_url()."assets/js/jquery-3.2.1.js");?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#periode").val("<?php print_r(date("Y"));?>");
        });

        $("#periode").change(function(){
            var periode = $("#periode").val();
            var dinas   = $("#dinas").val();

            var data_main = new FormData();
            data_main.append('periode', periode);
            data_main.append('dinas' , dinas);

            $.ajax({
                url: "<?php echo base_url()."front/Frontmain/get_data/";?>", // point to server-side PHP script 
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);

                    $("#out_table").html(res);
                    // response_add_dinas(res);
                }
            });

        });

        $("#dinas").change(function(){
            var periode = $("#periode").val();
            var dinas   = $("#dinas").val();

            var data_main = new FormData();
            data_main.append('periode', periode);
            data_main.append('dinas' , dinas);

            $.ajax({
                url: "<?php echo base_url()."front/Frontmain/get_data/";?>", // point to server-side PHP script 
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);

                    $("#out_table").html(res);
                    // response_add_dinas(res);
                }
            });
        });
    </script>

    