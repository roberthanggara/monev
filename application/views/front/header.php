<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Malang Open Data</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>web_new/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>web_new/img/apple-touch-icon-114x114.png">

    

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/fonts/font-awesome/css/font-awesome.css">

    <!-- Slider
    ================================================== -->
    <link href="<?php echo base_url(); ?>web_new/css/owl.carousel.css" rel="stylesheet" media="screen">
    <link href="<?php echo base_url(); ?>web_new/css/owl.theme.css" rel="stylesheet" media="screen">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/nivo-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>web_new/css/nivo-lightbox/default.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <script src="<?php echo base_url(); ?>web_new/js/jssor.slider-27.5.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideshowTransitions = [{
                $Duration: 800,
                x: 0.3,
                $During: {
                    $Left: [0.3, 0.7]
                },
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                x: -0.3,
                $SlideOut: true,
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                x: -0.3,
                $During: {
                    $Left: [0.3, 0.7]
                },
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                x: 0.3,
                $SlideOut: true,
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                y: 0.3,
                $During: {
                    $Top: [0.3, 0.7]
                },
                $Easing: {
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                y: -0.3,
                $SlideOut: true,
                $Easing: {
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                y: -0.3,
                $During: {
                    $Top: [0.3, 0.7]
                },
                $Easing: {
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                y: 0.3,
                $SlideOut: true,
                $Easing: {
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                x: 0.3,
                $Cols: 2,
                $During: {
                    $Left: [0.3, 0.7]
                },
                $ChessMode: {
                    $Column: 3
                },
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                x: 0.3,
                $Cols: 2,
                $SlideOut: true,
                $ChessMode: {
                    $Column: 3
                },
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                y: 0.3,
                $Rows: 2,
                $During: {
                    $Top: [0.3, 0.7]
                },
                $ChessMode: {
                    $Row: 12
                },
                $Easing: {
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                y: 0.3,
                $Rows: 2,
                $SlideOut: true,
                $ChessMode: {
                    $Row: 12
                },
                $Easing: {
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                y: 0.3,
                $Cols: 2,
                $During: {
                    $Top: [0.3, 0.7]
                },
                $ChessMode: {
                    $Column: 12
                },
                $Easing: {
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                y: -0.3,
                $Cols: 2,
                $SlideOut: true,
                $ChessMode: {
                    $Column: 12
                },
                $Easing: {
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                x: 0.3,
                $Rows: 2,
                $During: {
                    $Left: [0.3, 0.7]
                },
                $ChessMode: {
                    $Row: 3
                },
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                x: -0.3,
                $Rows: 2,
                $SlideOut: true,
                $ChessMode: {
                    $Row: 3
                },
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                x: 0.3,
                y: 0.3,
                $Cols: 2,
                $Rows: 2,
                $During: {
                    $Left: [0.3, 0.7],
                    $Top: [0.3, 0.7]
                },
                $ChessMode: {
                    $Column: 3,
                    $Row: 12
                },
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                x: 0.3,
                y: 0.3,
                $Cols: 2,
                $Rows: 2,
                $During: {
                    $Left: [0.3, 0.7],
                    $Top: [0.3, 0.7]
                },
                $SlideOut: true,
                $ChessMode: {
                    $Column: 3,
                    $Row: 12
                },
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Top: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                $Delay: 20,
                $Clip: 3,
                $Assembly: 260,
                $Easing: {
                    $Clip: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                $Delay: 20,
                $Clip: 3,
                $SlideOut: true,
                $Assembly: 260,
                $Easing: {
                    $Clip: $Jease$.$OutCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                $Delay: 20,
                $Clip: 12,
                $Assembly: 260,
                $Easing: {
                    $Clip: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 800,
                $Delay: 20,
                $Clip: 12,
                $SlideOut: true,
                $Assembly: 260,
                $Easing: {
                    $Clip: $Jease$.$OutCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }];

            var jssor_1_options = {
                $AutoPlay: 1,
                $SlideshowOptions: {
                    $Class: $JssorSlideshowRunner$,
                    $Transitions: jssor_1_SlideshowTransitions,
                    $TransitionsOrder: 1
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,
                    $SpacingX: 5,
                    $SpacingY: 5
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                } else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <style>
        /*jssor slider loading skin spin css*/
        
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }
        
        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }
        /*jssor slider arrow skin 106 css*/
        
        .jssora106 {
            display: block;
            position: absolute;
            cursor: pointer;
        }
        
        .jssora106 .c {
            fill: #fff;
            opacity: .3;
        }
        
        .jssora106 .a {
            fill: none;
            stroke: #000;
            stroke-width: 350;
            stroke-miterlimit: 10;
        }
        
        .jssora106:hover .c {
            opacity: .5;
        }
        
        .jssora106:hover .a {
            opacity: .8;
        }
        
        .jssora106.jssora106dn .c {
            opacity: .2;
        }
        
        .jssora106.jssora106dn .a {
            opacity: 1;
        }
        
        .jssora106.jssora106ds {
            opacity: .3;
            pointer-events: none;
        }
        /*jssor slider thumbnail skin 101 css*/
        
        .jssort101 .p {
            position: absolute;
            top: 0;
            left: 0;
            box-sizing: border-box;
            background: #000;
        }
        
        .jssort101 .p .cv {
            position: relative;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            border: 2px solid #000;
            box-sizing: border-box;
            z-index: 1;
        }
        
        .jssort101 .a {
            fill: none;
            stroke: #fff;
            stroke-width: 400;
            stroke-miterlimit: 10;
            visibility: hidden;
        }
        
        .jssort101 .p:hover .cv,
        .jssort101 .p.pdn .cv {
            border: none;
            border-color: transparent;
        }
        
        .jssort101 .p:hover {
            padding: 2px;
        }
        
        .jssort101 .p:hover .cv {
            background-color: rgba(0, 0, 0, 6);
            opacity: .35;
        }
        
        .jssort101 .p:hover.pdn {
            padding: 0;
        }
        
        .jssort101 .p:hover.pdn .cv {
            border: 2px solid #fff;
            background: none;
            opacity: .35;
        }
        
        .jssort101 .pav .cv {
            border-color: #fff;
            opacity: .35;
        }
        
        .jssort101 .pav .a,
        .jssort101 .p:hover .a {
            visibility: visible;
        }
        
        .jssort101 .t {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            border: none;
            opacity: .6;
        }
        
        .jssort101 .pav .t,
        .jssort101 .p:hover .t {
            opacity: 1;
        }
    </style>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <!-- Navigation
    ==========================================-->
    <nav id="menu" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <!-- <a class="navbar-brand page-scroll" href="#about"><img src="<?php echo base_url(); ?>web_new/img/logo.png" alt=" " height="40" width="150"></a>  -->
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo base_url(); ?>beranda" class="page-scroll">Beranda</a></li>

                    <li><a href="<?php echo base_url(); ?>variabel_kepatuahan" class="page-scroll">Variabel Kepatuhan</a></li>
                    <li><a href="#" class="page-scroll">Rekap Hasil</a></li>
                    <li><a href="#" class="page-scroll">Rekap Total Hasil</a></li>
                    <li><a href="#" class="page-scroll">Rekap Hasil Evaluasi</a></li>
                    <!-- <li><a href="#contact" class="page-scroll">Contact</a></li> -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <!-- Header -->