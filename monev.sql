-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14 Jun 2019 pada 02.26
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `monev`
--

DELIMITER $$
--
-- Fungsi
--
CREATE DEFINER=`root`@`localhost` FUNCTION `func_insert_admin`(`nama` VARCHAR(100), `email` VARCHAR(50), `nip` VARCHAR(25), `jabatan` VARCHAR(50), `id_bidang` VARCHAR(12), `lv` INT(2), `pass` VARCHAR(32), `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS varchar(14) CHARSET latin1
    NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, '0', lv, email, pass, '0', nama, nip, jabatan, id_bidang, '0', '0000-00-00 00:00:00', admin_del, time_update);
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` char(12) NOT NULL,
  `del` enum('0','1') NOT NULL,
  `id_lv` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_bidang` char(12) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `del`, `id_lv`, `email`, `password`, `status_active`, `nama`, `nip`, `jabatan`, `id_bidang`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('AD2019020001', '0', 1, 'kominfo_super@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Suwardi Surya Ningrat', '19091999012130', 'staff bidang penelitian', '3', '0', '0000-00-00 00:00:00', '0', '2019-02-21 00:00:00'),
('AD2019020002', '0', 2, 'sip@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Raditya Dika', '12097561409', 'Pelaksana Tugas', '20', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 04:23:13'),
('AD2019020003', '0', 3, 'diskominfo_admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Daniel Fernando', '19031994092011', 'Staff Umum', '3', '0', '2019-02-27 02:47:20', 'AD2019020001', '2019-02-27 02:45:23'),
('AD2019020004', '0', 4, 'barenlitbang@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Didi Kemput', '14091995022003', 'Staff Bagian Penelitian Publik', '31', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 02:53:52'),
('AD2019020005', '0', 2, 'dummy@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'dinda arumi', '12121990031999', 'Pelaksana Tugas', '3', '0', '2019-02-27 04:13:16', 'AD2019020001', '2019-02-27 04:24:31'),
('AD2019020006', '0', 3, 'disperin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Doni Andreas', '12031994051998', 'staff administrasi', '2', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 04:58:11'),
('AD2019020007', '0', 3, 'disbudpar@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'Erick F', '12091985302000', 'staff tatausaha', '4', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 04:59:48'),
('AD2019030001', '0', 3, 'dinkes@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1', 'hendro', '12837921645', 'admin umum', '8', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-03-04 04:31:37'),
('AD2019060001', '0', 2, 'dika@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '0', 'sumardiono ara', '123123123', 'preman', '3', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-06-11 06:22:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_lv`
--

CREATE TABLE IF NOT EXISTS `admin_lv` (
  `id_lv` int(2) NOT NULL AUTO_INCREMENT,
  `ket` varchar(32) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_lv`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `admin_lv`
--

INSERT INTO `admin_lv` (`id_lv`, `ket`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(1, 'Admin Super', '0', '0000-00-00 00:00:00', '0', '2019-02-21 00:00:00'),
(2, 'Admin Dinas', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-27 02:42:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dinas`
--

CREATE TABLE IF NOT EXISTS `dinas` (
  `id_dinas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dinas` varchar(500) NOT NULL,
  `alamat` text NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_dinas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data untuk tabel `dinas`
--

INSERT INTO `dinas` (`id_dinas`, `nama_dinas`, `alamat`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(2, 'Dinas Perdagangan', 'Jl. Simp. Terusan Danau Sentani 3 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Dinas Komunikasi dan Informatika (DISKOMINFO)', 'Perkantoran Terpadu Gedung A Lt. 4 Malang, Jl. Mayjend. Sungkono Malang 65132', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, 'Dinas Kebudayaan dan Pariwisata (DISBUDPAR)', 'Museum Mpu Purwa, Jl. Sukarno Hatta B. 210 Malang 65142', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'Dinas Perindustrian (DISPERIN)', 'Perkantoran Terpadu Gedung A Lt.3, Jl. Mayjen Sungkono Malang 65132', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'Dinas Pendidikan (DISDIK)', 'Jl. Veteran 19 Malang 65145', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(8, 'Dinas Kesehatan (DINKES)', 'Jl. Simp. Laksda Adisucipto 45 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(9, 'Dinas Perhubungan (DISHUB)', 'Jl. Raden Intan 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(10, 'Dinas Pertanian dan Ketahanan Pangan', 'Jl. A. Yani Utara 202 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(11, 'Dinas Perumahan dan Kawasan Pemukiman (DISPERKIM)', 'Jl. Bingkil 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(14, 'Dinas Koperasi dan Usaha Mikro', 'Jl. Panji Suroso 18 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(15, 'Dinas Kepemudaan dan Olahraga (DISPORA)', 'Jl. Tenes Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(16, 'Dinas Kepedudukan dan Pencatatan Sipil (DISPENDUKCAPIL)', 'Perkantoran Terpadu Gedung A Lt. 2, Jl. Mayjen Sungkono Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(17, 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana (DP3AP2KB)', 'Jl. Ki Ageng Gribig Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(18, 'Dinas Lingkungan Hidup (DLH)', 'Jl. Mojopahit Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(19, 'Dinas Perpustakaan Umum dan Arsip Daerah', 'Jl. Besar Ijen No.30a Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(20, 'Badan Kesatuan Bangsa dan Politik (BAKESBANGPOL)', 'Jl. Jend. A. Yani 98 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(33, 'Badan Kepegawaian Daerah', 'Jl. Tugu 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(26, 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPMPTSP)', 'Perkantoran Terpadu Gedung A Lt. 2 Malang,\r\nJl. Mayjend. Sungkono Malang 65132', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(27, 'Dinas Tenaga Kerja (DISNAKER)', 'Perkantoran Terpadu Gedung B Lt.3, Jl. Mayjen Sungkono Malang 65132', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(28, 'Dinas Pekerjaan Umum dan Penataan Ruang (DPUPR)', ' Jl. Bingkil 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(29, 'Dinas Sosial (DINSOS)', 'Jl. Sulfat No. 12 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(30, 'Inspektorat', 'Jl. Gajah Mada No. 2A Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(31, 'Badan Perencanaan, Penelitian dan Pengembangan (Barenlitbang)', 'Jl. Tugu No. 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(32, 'Badan Pelayanan Pajak Daerah (BP2D)', 'Perkantoran Terpadu Gedung B Lt.1 Jl. Mayjen Sungkono Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(34, 'Badan Pengelola Keuangan dan Aset Daerah (BPKAD)', 'Jl. Tugu 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(35, 'Badan Penanggulangan Bencana Daerah (BPBD)', 'Jl. Jend. A. Yani 98 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(36, 'Satuan Polisi Pamong Praja', 'Jl. Simpang Mojopahit No 1 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(37, 'Kecamatan Klojen', 'Jl. Surabaya 6 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(38, 'Kecamatan Blimbing', 'Jl. Raden Intan Kav. 14 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(39, 'Kecamatan Lowokwaru', 'Jl. Cengger Ayam I/12 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(40, 'Kecamatan Sukun', 'Jl. Keben I Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(41, 'Kecamatan Kedungkandang', 'Jl. Mayjen Sungkono 59 Malang', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(42, 'Badan Pengawas Pemilihan Umum (BAWASLU)', 'Jl. Teluk Cendrawasih No.206, Arjosari, Blimbing, Kota Malang, Jawa Timur 65126', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(43, 'Dewan Perwakilan Rakyat Daerah Kota Malang (DPRD)', 'Jl. Tugu No.1A, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(44, 'Rumah Potong Hewan Kota Malang', 'Jl. Kol. Sugiono No. 176, Malang, Jawa Timur 65148', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(45, 'PDAM Kota Malang ', 'Jl. Danau Sentani Raya No.100, Madyopuro, Kedungkandang, Kota Malang, Jawa Timur 65142', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(46, 'Komisi Pemilihan Umum Kota Malang (KPU) ', 'Jl. Bantaran No.6, Purwantoro, Blimbing, Kota Malang, Jawa Timur 65126', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(51, 'Bank Perkreditan Rakyat', 'JL. Borobudur no. 18', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-28 09:48:41'),
(50, 'dinas perbuatan tidak menyenangkan', 'Malang', '1', '2019-02-27 03:35:41', 'AD2019020001', '2019-02-27 03:35:24'),
(52, 'surya', 'surya', '1', '2019-03-04 02:58:07', 'AD2019020001', '2019-03-01 03:57:29'),
(53, 'dinas keamanan sekali', 'malang selatan', '1', '2019-06-11 06:23:56', 'AD2019020001', '2019-06-11 06:23:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `doc`
--

CREATE TABLE IF NOT EXISTS `doc` (
  `id_doc` int(11) NOT NULL AUTO_INCREMENT,
  `id_admin` varchar(12) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `url_upload` text NOT NULL,
  `periode` varchar(4) NOT NULL,
  `time_upload` datetime NOT NULL,
  `sts_check` enum('0','1','2') NOT NULL,
  `admin_check` varchar(12) NOT NULL,
  `time_check` datetime NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `admin_update` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_doc`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `doc`
--

INSERT INTO `doc` (`id_doc`, `id_admin`, `id_jenis`, `url_upload`, `periode`, `time_upload`, `sts_check`, `admin_check`, `time_check`, `is_delete`, `admin_update`, `time_update`) VALUES
(1, 'AD2019020002', 1, '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b.pdf', '2019', '2019-06-11 00:00:00', '1', 'AD2019020001', '2019-06-14 01:56:13', '0', 'AD2019020002', '2019-06-13 02:53:23'),
(2, 'AD2019020002', 2, 'd4735e3a265e16eee03f59718b9b5d03019c07d8b6c51f90da3a666eec13ab35.pdf', '2019', '2019-06-11 00:00:00', '2', 'AD2019020001', '2019-06-14 01:56:33', '0', 'AD2019020002', '2019-06-13 02:50:46'),
(3, 'AD2019020002', 3, '4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce.pdf', '2019', '2019-06-14 12:56:17', '1', 'AD2019020001', '2019-06-14 01:56:27', '0', '', '2019-06-14 12:56:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_doc`
--

CREATE TABLE IF NOT EXISTS `jenis_doc` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan_jenis` text NOT NULL,
  `point` double NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL,
  PRIMARY KEY (`id_jenis`),
  UNIQUE KEY `id_permohonan` (`id_jenis`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `jenis_doc`
--

INSERT INTO `jenis_doc` (`id_jenis`, `keterangan_jenis`, `point`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(1, 'Skripsi', 10, '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(2, 'Tesis', 20.5, '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(3, 'Disertasi', 1, '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(4, 'Karya Tulis Ilmiah', 1, '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(5, 'Program Kreativitas Mahasiswa', 2, '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(6, 'Praktek Kerja Lapang / Magang', 2, '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(7, 'Pengabdian Masyarakat', 2, '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(8, 'Praktek Kerja industri', 2, '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
(13, 'Skripsick', 2, '1', '2019-02-27 03:34:55', 'AD2019020001', '2019-02-27 03:34:40'),
(14, 'surya', 1, '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-06-11 08:01:02'),
(15, 'doni', 1, '1', '2019-06-11 08:11:27', 'AD2019020001', '2019-06-11 08:09:27'),
(16, 'disaster', 222, '1', '2019-06-11 09:50:05', 'AD2019020001', '2019-06-11 09:49:52');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
